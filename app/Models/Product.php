<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Traits\uniquecheck;
//require_once('traits/uniquecheck.php');
class Product extends Model {

   protected $table = 'zarest_products';
   protected $allowedFields = ['code','name','category','cost','tax','description','price','photo','photothumb','color','created_at','modified_at','type','alertqt','supplier','unit','taxmethod','h_stores','options'];
  //  static $validates_uniqueness_of = array(
  //     array('code')
  //  );
  use uniquecheck;
  public function checkvalidate() {
       $this->uniquecheck(array('code','message' => 'Can\'t have duplicate code.'));
   }

}
