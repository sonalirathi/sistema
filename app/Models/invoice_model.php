<?php
namespace App\Models;
use CodeIgniter\Model;
class Invoice_model extends Model
{

    protected $table = 'zarest_sales';
    protected $allowedFields = ['id','client_id','clientname','tax','discount','subtotal','total','created_by','modified_at','created_by','totalitems','status','paid','paidmethod','taxamount','discountamount','register_id','firstpayement','waiter_id'];

}
