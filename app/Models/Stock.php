<?php
namespace App\Models;
use CodeIgniter\Model;
class Stock extends Model {

   protected $table = 'zarest_stocks';
   protected $allowedFields = ['id','product_id','type','store_id','warehouse_id','quantity','price'];

}
