<?php
namespace App\Models;
use CodeIgniter\Model;
class Payement extends Model {
    
   protected $table = 'zarest_payements';
   protected $allowedFields = ['date','paid','paidmethod','created_by','register_id','sale_id','waiter_id'];
}
