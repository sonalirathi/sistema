<?php
namespace App\Models;
use CodeIgniter\Model;

class Setting extends Model {

   protected $table = 'zarest_settings';
   protected $allowedFields = ['companyname','logo','phone','currency','keyboard','receiptheader','receiptfooter','theme','discount','tax','timezone','language','stripe','stripe_secret_key','stripe_publishable_key','decimals'];
}
