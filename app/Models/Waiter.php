<?php
namespace App\Models;
use CodeIgniter\Model;
class Waiter extends Model {

   protected $table = 'zarest_waiters';
   protected $allowedFields = ['name','phone','email','created_at','store_id'];
}
