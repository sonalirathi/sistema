<?php
namespace App\Models;
use CodeIgniter\Model;
class Combo_item extends Model {

    protected $table = 'zarest_combo_items';
    protected $allowedFields = ['product_id','item_id','quantity'];

}
