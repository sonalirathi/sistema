<?php
namespace App\Models;
use CodeIgniter\Model;
class Table extends Model {

   protected $table = 'zarest_tables';
    protected $allowedFields = ['name','zone_id','status','time','store_id','checked'];
}
