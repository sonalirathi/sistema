<?php
namespace App\Models;
use CodeIgniter\Model;
class Posale extends Model {

   protected $table = 'zarest_posales';
   protected $allowedFields = ['id','product_id','name','price','register_id','number','table_id','options','status','qt','time'];
}
