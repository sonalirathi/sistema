<?php
namespace App\Models;
use CodeIgniter\Model;
class Supplier extends Model {

   protected $table = 'zarest_suppliers';
   protected $allowedFields = ['name','phone','email','note','created_at'];
}
