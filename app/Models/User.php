<?php
namespace App\Models;
use CodeIgniter\Model;

class User extends Model
{
    protected $table = 'zarest_users';
    protected $allowedFields = ['username','firstname','lastname','hashed_password','email','role','last_active','avatar','created_at','store_id'];
    var $password = FALSE;

    function before_save()
    {
        if($this->password)
        $this->hashed_password = $this->hash_password($this->password);
    }

    function set_password($plaintext)
    {

            $this->hashed_password = $this->hash_password($plaintext);
    }
    private function hash_password($password)
    {
            $salt = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
            $hash = hash('sha256', $salt . $password);

            return $salt . $hash;
    }

    private static function validate_password($hashed_password, $password)
    {
            $salt = substr($hashed_password, 0, 64);
            $hash = substr($hashed_password, 64, 64);

            $password_hash = hash('sha256', $salt . $password);
            $password_hash = '';
            return $password_hash == $hash;
    }

    public static function validate_login($CI, $username, $password)
    {
            $userModel = new User();
            $user = $userModel->where('username', $_POST['username'])->first();

            if($user && User::validate_password($user['hashed_password'],$password))
            {
                User::login($CI, $user);
                return $user;
            } else {
                    return FALSE;
            }
    }

    public static function login($CI, $user)
    {
        $ses_data = [
            'user_id'       => $user['id'],
            'user_name'     => $user['username'],
            'user_email'    => $user['email'],
            'user_role'    => $user['role'],
            'store_id'    => $user['store_id'],
            'avatar'    => $user['avatar'],
            'firstname'    => $user['firstname'],
            'lastname'    => $user['lastname'],
            'logged_in'     => TRUE
        ];
        $CI->session->set($ses_data);
    }

    public static function logout($CI)
    {
        $CI->session->destroy();
        return true;
    }
    
}
