<?php
namespace App\Models;
use CodeIgniter\Model;
class Warehouse extends Model {

   protected $table = 'zarest_warehouses';
   protected $allowedFields = ['name','phone','email','adresse','created_at'];

}
