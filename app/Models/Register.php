<?php
namespace App\Models;
use CodeIgniter\Model;
class Register extends Model {

    protected $table = 'zarest_registers';
    protected $allowedFields = ['date','cash_total','cash_sub','cc_total','cc_sub','cheque_total','cheque_sub','note','closed_at','closed_by','status','user_id','cash_inhand','waiterscih','store_id'];

    function getRegisterReport($store_id, $start, $end){
        $query = "SELECT * FROM `zarest_registers` WHERE store_id = '$store_id' AND date between '$start' AND '$end' ORDER BY date";
        $query=$this->db->query($query);
        return $query->getResultArray();
    }
}
