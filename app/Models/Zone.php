<?php
namespace App\Models;
use CodeIgniter\Model;
class Zone extends Model {

   protected $table = 'zarest_zones';
   protected $allowedFields = ['store_id','name'];
}
