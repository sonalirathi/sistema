<?php
namespace App\Models;
use CodeIgniter\Model;
class Hold extends Model {

   protected $table = 'zarest_holds';
   protected $allowedFields = ['number','time','table_id','register_id','customer_id','waiter_id'];
}
