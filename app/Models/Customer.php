<?php
namespace App\Models;
use CodeIgniter\Model;
class Customer extends Model {

   protected $table = 'zarest_customers';
   protected $allowedFields = ['name','phone','email','discount','created_at'];
}
