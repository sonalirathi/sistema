<?php
namespace App\Models;
use CodeIgniter\Model;
class Category extends Model {

    protected $table = 'zarest_categories';
    protected $allowedFields = ['name','created_at'];
}
