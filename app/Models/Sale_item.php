<?php
namespace App\Models;
use CodeIgniter\Model;
class Sale_item extends Model {

    protected $table = 'zarest_sale_items';
    protected $allowedFields = ['sale_id','product_id','name','price','qt','subtotal','date'];
    
    function getSaleItemProductReport($product_id, $start, $end){
        $query = "SELECT * FROM `zarest_sale_items` WHERE product_id = '$product_id' AND date between '$start' AND '$end' ORDER BY date";
        $query=$this->db->query($query);
        return $query->getResultArray();

    }
}
