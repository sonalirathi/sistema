<?php
namespace App\Models;
use CodeIgniter\Model;
class Store extends Model {

   protected $table = 'zarest_stores';
   protected $allowedFields = ['name','email','phone','adresse','footer_text','city','country','created_at','status'];

}
