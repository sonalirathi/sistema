<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('auth');
$routes->setDefaultMethod('login');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'auth::login');
$routes->get('lang/{locale}', 'Language::index');
$routes->post('login','auth::login');
$routes->get('dashboard','dashboard::index');
$routes->get('logout','auth::logout');
$routes->get('kitchens','Kitchens::index');
$routes->get('sales','sales::index');
$routes->get('expences','Expences::index');
$routes->get('settings','settings::index');
$routes->post('settings/addUser','settings::addUser');
$routes->post('settings/updateSettings','settings::updateSettings');
$routes->get('settings/editUser/(:any)','settings::editUser/$1');
$routes->post('settings/editUser/(:any)','settings::editUser/$1');
$routes->get('settings/deleteUser/(:any)','settings::deleteUser/$1');
$routes->get('warehouses/edit/(:any)','warehouses::edit/$1');
$routes->post('warehouses/add','warehouses::add');
$routes->post('warehouses/edit/(:any)','warehouses::edit/$1');
$routes->get('warehouses/delete/(:any)','warehouses::delete/$1');
$routes->get('stats','stats::index');
$routes->get('waiters','waiters::index');
$routes->post('waiters/edit/(:any)','waiters::edit/$1');
$routes->get('waiters/edit/(:any)','waiters::edit/$1');
$routes->get('waiters/delete/(:any)','waiters::delete/$1');
$routes->post('waiters/add/','waiters::add');

$routes->get('customers','customers::index');
$routes->post('customers/edit/(:any)','customers::edit/$1');
$routes->get('customers/edit/(:any)','customers::edit/$1');
$routes->get('customers/delete/(:any)','customers::delete/$1');
$routes->post('customers/add/','customers::add');

$routes->get('suppliers','suppliers::index');
$routes->post('suppliers/edit/(:any)','suppliers::edit/$1');
$routes->get('suppliers/edit/(:any)','suppliers::edit/$1');
$routes->get('suppliers/delete/(:any)','suppliers::delete/$1');
$routes->post('suppliers/add/','suppliers::add');

$routes->get('products','products::index');
$routes->post('products','products::index');
$routes->post('products/edit/(:any)','products::edit/$1');
$routes->get('products/edit/(:any)','products::edit/$1');
$routes->get('products/delete/(:any)','products::delete/$1');
$routes->post('products/add/','products::add');
$routes->get('products/csv','products::csv');
$routes->post('products/importcsv','products::importcsv');
$routes->post('products/updatestock','products::updatestock');
//$routes->get('productcontroller/Viewproduct','productcontroller::Viewproduct');
$routes->post('productcontroller/Viewproduct/(:any)','productcontroller::Viewproduct/$1');
$routes->post('productcontroller/add/','productcontroller::add');
$routes->get('productcontroller/suggest','productcontroller::suggest');
$routes->get('productcontroller/addcombo','productcontroller::addcombo');
$routes->get('productcontroller/modifystock/(:any)','productcontroller::modifystock/$1');
$routes->post('productcontroller/makePrdInvis/(:any)/(:any)','productcontroller::makePrdInvis/$1/$2');
$routes->post('productcontroller/barcode/(:any)/(:any)/(:any)','productcontroller::barcode/$1/$2/$3');

$routes->get('categories','categories::index');
$routes->post('categories/edit/(:any)','categories::edit/$1');
$routes->get('categories/edit/(:any)','categories::edit/$1');
$routes->get('categories/delete/(:any)','categories::delete/$1');
$routes->post('categories/add/','categories::add');

$routes->get('categorie_expences','categorie_expences::index');
$routes->post('categorie_expences/edit/(:any)','categorie_expences::edit/$1');
$routes->get('categorie_expences/edit/(:any)','categorie_expences::edit/$1');
$routes->get('categorie_expences/delete/(:any)','categorie_expences::delete/$1');
$routes->post('categorie_expences/add/','categorie_expences::add');

$routes->get('stores','Stores::index');
$routes->post('stores/edit/(:any)','Stores::edit/$1');
$routes->get('stores/edit/(:any)','Stores::edit/$1');
$routes->get('stores/delete/(:any)','Stores::delete/$1');
$routes->post('stores/add/','Stores::add');
$routes->get('stores/editStoreTable/(:any)','Stores::editStoreTable/$1');
$routes->post('stores/editStoreTable/(:any)','Stores::editStoreTable/$1');
$routes->post('stores/addzone','Stores::addzone');
$routes->post('stores/editzone','Stores::editzone');
$routes->post('stores/addTable','Stores::addTable');
//$routes->post('stores/editTable','Stores::editTable');
$routes->post('stores/deletezone/(:any)/(:any)','Stores::deletezone/$1/$2');

$routes->post('stores/deletetable','Stores::deletetable');

$routes->get('stores/editTables/(:any)','Stores::editTables/$1');
$routes->post('stores/editTables/(:any)','Stores::editTables/$1');

$routes->post('pos/AddNewSale/(:any)','Pos::AddNewSale/$1');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
