<?php
function facturar($pedidoId, $items)
{
	$ruc = "214099140015";
	$security = true;
	$url = "http://www.solucionfactura.com/desawebsvcapi/api/document/savecomplete";
	$requestHttpMethod = "POST";
	$appId = "5d53bce03ec34c0a911182d4c228ee6c";
	$apiKey = "E93reRTUJHsCuQSHR+L3GxqOJyDmQpCgps102ciuabc=";
	$nonce = strtolower(str_replace("-", "", $pedidoId));
	$requestTimeStamp = time();
$error_msg= "error desconocido";
	$datosFactura = new stdClass();
	// se crear el objeto json
	
	$datos = json_encode($datosFactura);
	$requestUri = strtolower(urlencode($url));
	$requestContentBase64String = base64_encode(md5($datos, true));

	$signatureRawData = "$appId$requestHttpMethod$requestUri$requestTimeStamp$nonce$requestContentBase64String";
	$signatureRawData = "$appId$requestHttpMethod$requestUri$requestTimeStamp$nonce$requestContentBase64String";

	$secretKeyByteArray = base64_decode($apiKey);

	$signature = hash_hmac("sha256", $signatureRawData, $secretKeyByteArray, true);
	$requestSignatureBase64String = base64_encode($signature);
	$authorization = "$appId:$requestSignatureBase64String:$nonce:$requestTimeStamp";
	
	$headers = array();
	$headers[] = "Content-type: application/json";
	$headers[] = "Authorization: amx $authorization";
	$headers[] = "ClientId: $pedidoId";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $datos);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	$server_output = curl_exec($ch);
	if (curl_errno($ch)) {
		$error_msg = curl_error($ch);
	}
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$header = substr($server_output, 0, $header_size);
	$body = substr($server_output, $header_size);
	curl_close($ch);
	$result = substr(strstr($header, " "), 1);
	if (substr($result, 0, 3) != 200 && !$error_msg)
	{
		$error_msg = substr(strstr($result, " "), 1);
		$error_msg = strstr($result, " ", true) . " " . strstr($error_msg, "\r", true);
		if ($body)
			$error_msg .= " - " . $body;
	}

	return $error_msg ? "ERROR: " . $error_msg : $body;
}