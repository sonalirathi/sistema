<script type="text/javascript">
$(document).ready(function() {$('#ticket').modal({show: false, backdrop: 'static', keyboard: false});});

var id = setTimeout(function(){
 window.location.reload(1);
}, 15000);

function showticket(table){
   $('#printSection').load("<?php echo site_url('pos/showticketKit')?>/"+table);
   clearTimeout(id);
   $('#ticket').modal('show');
}
function PrintTicket() {
   $('.modal-body').removeAttr('id');
   window.print();
   $('.modal-body').attr('id', 'modal-body');
}
function closeModal() {
  window.location.reload(1);
}
</script>
<!-- Page Content -->
<div class="container">
   <div class="row" style="margin-top:50px;">
        <?=!$zones ? '<h4 style="margin-top:60px">'.label("NoTables").'</h4>' : '';?>
        <?php foreach ($zones as $zone):?>
        <div class="row">
           <h1 class="choose_store"> <?=$zone['name'];?> </h1>
        </div>
        <div class="row tablesrow">
           <?php foreach ($tables as $table):?>
              <?php if($table['zone_id'] == $zone['id']) {?>
           <div class="col-sm-2 col-xs-4 "  >
              <?php if($table['time'] == 'n'){?><?php } ?>
        
                <?=$table['name'];?>
                
                <?php
                
            $table = $table['id'];
           //$ref = $table['name'];
            
      
      $posales = $PosaleModel->where('table_id',$table)->find();
      
      foreach ($posales as $posale) {
        $d1 = new DateTime($posale['time']);
        $d2 = new DateTime($posale['time']);
        if($d1 == $d2){
          $posale['time'] = 'y';
        }else{
          $posale['time'] = 'n';
        }
      }
      //$table['checked'] = date("Y-m-d H:i:s");
      //$table->save();


   $ticket = '<div >
            <span style="font-weight: 800; font-size: 13px;">Hora: ' . $table. '</span><br>
            <span style="font-weight: 800; font-size: 14px;">Staff: &nbsp' . $table . '</span><br>
            <span style="font-weight: 800; font-size: 18px;">Ref:&nbsp' . $table . '</span><br>
            <span style="font-weight: 800; font-size: 18px;">' . label("Customer") . ': ' . $table . '</span>
            </div>
            
            <table class="table" cellspacing="0" border="0">
            <tr>
            <td style="text-align:left; border-bottom:1px solid #000; border-top:1px solid #000;" ><h4>' . label("Product") . '</h4></td>
            <td style="text-align:center; border-bottom:1px solid #000; border-top:1px solid #000;"><h4>Cant</h4></td>
            <td style="text-align:right; border-bottom:1px solid #000; border-top:1px solid #000;"><h4>Importe</h4></td>
            </tr>';

      $i = 1;
      foreach ($posales as $posale) {
           $ticket .= '<tr style="'.($posale['time'] == "n" ? 'background-color:#FFC0CB;' : '').'"><td style="text-align:center; width:30px;"></td>
           <td style="text-align:left; width:180px;">' . $posale['name'] . '<br><span style="font-size:12px;color:#666">'.$posale['options'].'</span></td>
           <td style="text-align:center; width:50px;">' . $posale['qt'] . '</td><td style="text-align:right; width:70px;font-size:14px; ">' . number_format((float)($posale['qt'] * $posale['price']), $setting['decimals'], '.', '') . ' ' . $setting['currency'] . '</td></tr>';
           $i ++;
      }

      $ticket .= '</table>';


      echo $ticket;    
            
            
            ?>
            
            
            
          
           </div>
           <?php } ?>
           <?php endforeach;?>
        </div>
     <?php endforeach;?>


</div>
<!-- /.container -->

<!-- Modal ticket -->
<div class="modal fade" id="ticket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document" id="ticketModal">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="ticket"><?=label("Receipt");?></h4>
      </div>
      <div class="modal-body" id="modal-body">
         <div id="printSection">
            <!-- Ticket goes here -->
            <center><h1 style="color:#34495E"><?=label("empty");?></h1></center>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default hiddenpr" onclick="closeModal()"><?=label("Close");?></button>
        <button type="button" class="btn btn-add hiddenpr" onclick="PrintTicket()"><?=label("print");?></button>
      </div>
    </div>
 </div>
</div>
<!-- /.Modal -->
