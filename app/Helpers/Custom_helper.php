<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function label($label) {
    $lang = service('request')->getLocale();
    $lb = lang($lang."_lang.".$label);
    if(!empty($lb)) {
       return $lb;
    } else {
       return $label;
    }
}

function p($p, $exit = 1)
{
    echo '<pre>';
    print_r($p);
    echo '</pre>';
    if ($exit == 1)
    {
        exit;
    }
}
function validate_password($ci, $password)
{
    $salt = substr($ci->hashed_password, 0, 64);
    $hash = substr($ci->hashed_password, 64, 64);

    $password_hash = hash('sha256', $salt . $password);
    $password_hash = '';
    return $password_hash == $hash;
}


