<?php

namespace App\Controllers;

use App\Models\Setting;
use App\Models\User;
use App\Models\Register;
use App\Models\Store;
use App\Models\Hold;
use App\Models\Table;
use App\Models\Posale;
use App\Models\Combo_item;
use App\Models\Product;
use App\Models\Stock;
use App\Models\Customer;
use App\Models\Sale;
use App\Models\Payement;
use App\Models\Waiter;
use App\Models\Sale_item;
use Zend\Barcode\Barcode;
use Zend\Barcode\Object\Code128;
use Zend\Barcode\Renderer\Image;

require_once APPPATH.'Libraries/dompdf/autoload.inc.php';

use Dompdf\Dompdf;
require_once APPPATH . 'Functions/efactura.php';
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\ErrorObject;

class Pos extends BaseController {

    protected $session;

    function __construct() {
//        parent::__construct();
    }

    public function findproduct($code) {
        $ProductModel = new Product();
        $product = $ProductModel->where('code',$code)->first();
        echo $product['id'];
    }

    public function openregister($id = 0, $userRole = null) {
        $lang = $this->session->get("lang") == null ? "english" : $this->session->get("lang");
        site_url('lang/'.$lang);
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->store = $this->session->get('store') ? $this->session->get('store') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $RegisterModel = new Register;
        $StoreModel = new Store;
        if ($_POST) {
            $cash = isset($_POST['cash']) ? $_POST['cash'] : NULL;
            $id = isset($_POST['store']) ? $_POST['store'] : NULL;
            $waitersCach = isset($_POST['waitersCach']) ? $_POST['waitersCach'] : NULL;
            p($_POST,1);
            $waitercc = '';
            foreach ($waitersCach as $key => $value) {
                $waitercc .= $value ? $key . ',' . $value . ',' : '';
            }
            $data = array(
                "status" => 1,
                "user_id" => $this->session->get('user_id'),
                "cash_inhand" => $cash,
                "waiterscih" => $waitercc,
                "store_id" => $id
            );

            $register = $RegisterModel->save($data);

            $store = $StoreModel->find($id);
            $store['status'] = 1;
            $store->save();
            $this->session->set('register', $register->id);
            $this->session->set('store', $id);
            redirect("", "location");
        }
        $conditions = array('store_id' => $id, 'status' => 1);
        $open_reg = $RegisterModel->where($conditions)->first();
        if(isset($open_reg['id'])){
            $this->session->set('register', $open_reg['id']);
        }
        $this->session->set('store', $id);

        if ($userRole === 'kitchen') {
            redirect("kitchens", "location");
        } else {
            return redirect()->route('dashboard');
//          redirect("dashboard", "location");
        }
    }

    public function selectTable($id) {
        $HoldModel = new Hold();
        $TableModel = new Table();
        $PosaleModel = new Posale();
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $conditionsHold = array('register_id' => $this->register, 'table_id' => $id);
        $hold = $HoldModel->where($conditionsHold)->first();

        if (!$hold) {
            $attributes = array(
                'number' => 1,
                'time' => date("H:i"),
                "table_id" => $id,
                'register_id' => $this->register
            );
            $HoldModel->insert($attributes);
        } else {
            $posaleData = [
                'status' => 1
            ];
            $posaleConditons = array('number' => 1, 'register_id' => $this->register, 'table_id' => $id);

            $PosaleModel->update($posaleConditons, $posaleData);
        }
        if ($id > 0) {
            $table = $TableModel->find($id);
            if ($table['status'] != 1) {
                $tableData['status'] = 1;
                $tableData['time'] = date("H:i");
                $table->update($id, $tableData);
            }
        }

        $this->session->set('selectedTable', $id . 'h');
        return redirect()->route('dashboard');
    }

    public function switshregister() {
        $this->session->set('register', 0);
        $this->session->set('store', 0);
        return redirect()->route('dashboard');
    }

    public function switshtable() {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $PosaleModel = new Posale();
        $posaleData = [
            'status' => 0
        ];
        $posaleConditons = array('status' => 1, 'register_id' => $this->register);

        $PosaleModel->update($posaleConditons, $posaleData);

        $this->session->set('selectedTable', 0);
        return redirect()->route('dashboard');
    }

    public function addpdc()
    {
        $StockModel = new Stock();
        $PosaleModel = new Posale();
        $RegisterModel = new Register();
        $ProductModel = new Product;
        $Combo_itemModel = new Combo_item();
        $StockModel = new Stock();
            
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        
        $product_id = isset($_POST['product_id']) ? $_POST['product_id'] : NULL ;
        
        $product = $ProductModel->find($product_id);
        $PostPrice = isset($_POST['price']) ? $_POST['price'] : NULL;
        $price = !$product['taxmethod'] || $product['taxmethod'] == '0' ? floatval($PostPrice) : floatval($PostPrice)*(1 + $product['tax'] / 100);
        /******************************************* sock version *************************************************************/
        if($product['type'] == '0')
        {
            $RegisterModel = new Register();
            
            $register = $RegisterModel->find($this->register);
            $stockCond = array('store_id' => $register['store_id'], 'product_id' => $product_id);
            $stock = $StockModel->where($stockCond)->first();
          
            $quantity = $stock ? $stock['quantity'] : 0;
            $poscond = array('status' => 1, 'register_id' => $this->register, 'product_id' => $product_id, 'table_id' => $this->selectedTable);
            $posale = $PosaleModel->where($poscond)->first();
           
            if ($posale) {
                if($posale['qt'] < $quantity) {
                    $data = array(
                        "product_id" => $product_id,
                        "name" => isset($_POST['name']) ? $_POST['name'] : NULL,
                        "price" => $price,
                        "number" => isset($_POST['number']) ? $_POST['number'] : NULL,
                        "register_id" => isset($_POST['registerid']) ? $_POST['registerid'] : NULL,
                        "table_id" => $this->selectedTable,
                        "qt" => 1,
                        "status" => 1,
                        "time" => date("Y-m-d H:i:s")
                    );
                    $PosaleModel->insert($data);
                    echo json_encode(array(
                        "status" => TRUE
                    ));
                }else {
                    echo 'stock';
                }
            } else if($quantity != 0){
//              $qty = $posale['qt'] ++;
              $data = array(
                  "product_id" => $product_id,
                  "name" => isset($_POST['name']) ? $_POST['name'] : NULL,
                  "price" => $price,
                  "number" => isset($_POST['number']) ? $_POST['number'] : NULL,
                  "register_id" =>isset($_POST['registerid']) ? $_POST['registerid'] : NULL,
                  "table_id" => $this->selectedTable,
                  "qt" => 1,
                  "status" => 1,
                  "time" => date("Y-m-d H:i:s")
              );
              $PosaleModel->insert($data);
              echo json_encode(array(
                  "status" => TRUE
              ));
          }else {
             echo 'stock';
          }
         /******************************************* combo version *************************************************************/
       }elseif ($product['type'] == '2') {
           $poCon = array('status' => 1,'register_id' => $this->register, 'product_id' => $product_id, 'table_id' => $this->selectedTable);
          $posale = $PosaleModel->where($poCon)->first();
          
          $register = $RegisterModel->find($this->register);
          $quantity = 1;
          
          $combos = $Combo_itemModel->where('product_id',$product_id)->find();
          
          foreach ($combos as $combo) {
             $prd = $ProductModel->find($combo['item_id']);
             if($prd['type'] == '0'){
                 $stockCondition = array('store_id' => $register['store_id'], 'product_id' => $combo['item_id']);
                 $stock = $StockModel->where($stockCondition)->first();
                 if ($posale)
                    $diff = $stock ? ($stock['quantity'] - $combo['quantity']*($posale['qt']+1)) : 1;
                 else
                   $diff = $stock ? ($stock['quantity'] - $combo['quantity']) : 1;
                $quantity = $stock ? ($diff >= 0 ? 1 : 0) : $quantity;
             }
          }
        if ($posale) {
            if($quantity > 0) {
             $posale['qt'] ++;
             $posale['time'] = date("Y-m-d H:i:s");
             $PosaleModel->update($posale);
             echo json_encode(array(
                 "status" => TRUE
             ));
          }else {
             echo 'stock';
          }
       } elseif($quantity > 0){
           $data = array(
                  "product_id" => $product_id,
                  "name" => isset($_POST['name']) ? $_POST['name'] : NULL,
                  "price" => $price,
                  "number" => isset($_POST['number']) ? $_POST['number'] : NULL,
                  "register_id" =>isset($_POST['registerid']) ? $_POST['registerid'] : NULL,
                  "table_id" => $this->selectedTable,
                  "qt" => 1,
                  "status" => 1,
                  "time" => date("Y-m-d H:i:s")
              );
              $PosaleModel->insert($data);
            
             echo json_encode(array(
                 "status" => TRUE
             ));
        }else {
            echo 'stock';
        }
       }
       /******************************************* service version *************************************************************/
       else {
           $poCon1 = array('status' => 1,'register_id' => $this->register, 'product_id' => $product_id, 'table_id' => $this->selectedTable);
            $posale = $PosaleModel->where($poCon1)->first();
          
          if ($posale) {
              $posale['qt'] ++;
              $posale['time'] = date("Y-m-d H:i:s");
              $PosaleModel->update($posale);
              echo json_encode(array(
                  "status" => TRUE
              ));
          } else {
              $data = array(
                  "product_id" => $product_id,
                  "name" => isset($_POST['name']) ? $_POST['name'] : NULL,
                  "price" => $price,
                  "number" => isset($_POST['number']) ? $_POST['number'] : NULL,
                  "register_id" =>isset($_POST['registerid']) ? $_POST['registerid'] : NULL,
                  "table_id" => $this->selectedTable,
                  "qt" => 1,
                  "status" => 1,
                  "time" => date("Y-m-d H:i:s")
              );
              $PosaleModel->insert($data);
            
              echo json_encode(array(
                  "status" => TRUE
              ));
          }
       }
    }

//******************************
//Carga productos en pantalla//
//***********************************

    public function load_posales() {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        
        $SettingModel = new Setting;
        $PosaleModel = new Posale;
        $ProductModel = new Product();
        $RegisterModel = new Register();
        $StockModel = new Stock();

        $setting = $SettingModel->select('currency')->first();

        $posCond = array('status' => 1,'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $posales = $PosaleModel->where($posCond)->find();
        
        $data = '';
        if ($posales) {
            foreach ($posales as $posale) {
                $ProductData = $ProductModel->select(['alertqt','type'])->find($posale['product_id']);
               
                $alertqt = $ProductData['alertqt'];
                $type = $ProductData['type'];
                
                $options = $posale['options'];
                $options = trim($options, ",");
                
                $storeid = $RegisterModel->select('store_id')->find($this->register);
                
                $storeid = $storeid['store_id'];
                
                $quantityCond = array('product_id' => $posale['product_id'], 'store_id' => $storeid);
                $stockQuantity = $StockModel->select('quantity')->where($quantityCond)->first();
                
                $stockQuantity = $stockQuantity['quantity'];
                
                $alert = $type == '0' ? ($stockQuantity - $posale['qt'] <= $alertqt ? 'background-color:pink' : '') : '';
                
                $row = '<div class="col-xs-12"><div class="panel panel-default product-details">
                <div class="panel-body" style="' . $alert . '"><div class="col-xs-5 nopadding">
                <div class="col-xs-2 nopadding"><a href="javascript:void(0)" onclick="delete_posale(' . "'" . $posale['id'] . "'" . ')">
                <span class="fa-stack fa-sm productD"><i class="fa fa-circle fa-stack-2x delete-product"></i><i class="fa fa-times fa-stack-1x fa-fw fa-inverse"></i></span>
                </a></div>
                <div class="col-xs-10 nopadding">
                <span><button type="button" onclick="addoptions(' . $posale['product_id'] . ', ' . $posale['id'] . ')" class="btn btn-success btn-xs" style="text-align:center;  font-size:18px;">' . $posale['name'] . '</button></span><br><span id="pooptions-' . $posale['id'] . '"> ' . $options . '</sapn></div></div>
                <div class="col-xs-2"><span class="textPD">' . number_format((float) $posale['price']) . ' ' . $setting['currency'] . '</span></div>
                <div class="col-xs-3 nopadding productNum"><a href="javascript:void(0)"><span class="fa-stack fa-sm decbutton">
                <i class="fa fa-square fa-stack-2x light-grey"></i>
                <i class="fa fa-minus fa-stack-1x fa-inverse white"></i></span></a>
                <input type="text" id="qt-' . $posale['id'] . '" onchange="edit_posale(' . $posale['id'] . ')" class="form-control" value="' . $posale['qt'] . '" placeholder="0" maxlength="3">
                <a href="javascript:void(0)"><span class="fa-stack fa-sm incbutton"><i class="fa fa-square fa-stack-2x light-grey"></i>
                <i class="fa fa-plus fa-stack-1x fa-inverse white"></i></span></a></div><div class="col-xs-2 nopadding ">
                <span class="subtotal textPD">' . $posale['price'] * $posale['qt'] . '  ' . $setting['currency'] . '</span></div></div> 
               </div></div>';

                $data .= $row;
            }
            // adding script for the +/- buttons
            $data .= '<script type="text/javascript">$(".incbutton").on("click", function() {var $button = $(this);var oldValue = $button.parent().parent().find("input").val();var newVal = parseFloat(oldValue) + 1;$button.parent().parent().find("input").val(newVal);edit_posale($button.parent().parent().find("input").attr("id").slice(3));});$(".decbutton").on("click", function() {var $button = $(this);var oldValue = $button.parent().parent().find("input").val();if (oldValue > 1) {var newVal = parseFloat(oldValue) - 1;} else {newVal = 1;}$button.parent().parent().find("input").val(newVal);edit_posale($button.parent().parent().find("input").attr("id").slice(3));});</script>';
        } else {

            $data = '<div class="messageVide">' . label("EmptyList") . ' </div>';
        }
        echo $data;
    }

    public function delete($id) {
        $PosaleModel = new Posale();
        $PosaleModel->delete($id);
        echo json_encode(array(
            "status" => TRUE
        ));
    }

    public function edit($id) {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        
        $PosaleModel = new Posale();
        $ProductModel = new Product();
        $RegisterModel = new Register();
        $StockModel = new Stock();
        $Combo_itemModel = new Combo_item();
        
        $posale = $PosaleModel->find($id);
        $product = $ProductModel->find($posale['product_id']);
        if ($product['type'] == '0') {
            $register = $RegisterModel->find($this->register);
            $stcokCond = array('store_id' => $register['store_id'], 'product_id' => $posale['product_id']);
            $stock = $StockModel->where($stcokCond)->first();
            $quantity = $stock ? $stock['quantity'] : 0;
            if (intval(isset($_POST['qt']) ? $_POST['qt'] : NULL) <= intval($quantity)) {

                $data = array(
                    "qt" => isset($_POST['qt']) ? $_POST['qt'] : NULL,
                    "time" => date('Y-m-d H:i:s')
                );
                $PosaleModel->update($id, $data);
                echo json_encode(array(
                    "status" => TRUE
                ));
            } else {
                echo 'stock';
            }
            /*             * ***************************************** combo version ************************************************************ */
        } elseif ($product['type'] == '2') {
            $register = $RegisterModel->find($this->register);
            $quantity = 1;
            $combos = $Combo_itemModel->where('product_id',$posale['product_id'])->find();
            foreach ($combos as $combo) {
                $prd = $ProductModel->find($combo->item_id);
                if ($prd->type == '0') {
                    $stockCodi = array('store_id' => $register['store_id'], 'product_id' => $combo['item_id']);
                    $stock = $StockModel->where($stockCodi)->first();
                    $diff = $stock ? ($stock['quantity'] - $combo['quantity'] * (isset($_POST['qt']) ? $_POST['qt'] : NULL)) : 1;
                    $quantity = $stock ? ($diff >= 0 ? 1 : 0) : $quantity;
                }
            }
            if ($quantity > 0) {
                $data = array(
                    "qt" => isset($_POST['qt']) ? $_POST['qt'] : NULL,
                    "time" => date('Y-m-d H:i:s')
                );
                $PosaleModel->update($id, $data);
                echo json_encode(array(
                    "status" => TRUE
                ));
            } else {
                echo 'stock';
            }
        } else {
            $data = array(
                "qt" => isset($_POST['qt']) ? $_POST['qt'] : NULL,
                "time" => date('Y-m-d H:i:s')
            );
            $PosaleModel->update($id, $data);
            echo json_encode(array(
                "status" => TRUE
            ));
        }
    }

    public function subtot() {
        $PosaleModel = new Posale();
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        $posalesConditions = array('status' => 1, 'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $posales = $PosaleModel->where($posalesConditions)->find();
        
        $sub = 0;
        foreach ($posales as $posale) {
            $sub += $posale['price'] * $posale['qt'];
        }
        echo $sub;
    }

    public function totiems() {
        $PosaleModel = new Posale();
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        
        $posalesConditions = array('status' => 1, 'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $posales = $PosaleModel->where($posalesConditions)->find();
        
        $sub = 0;
        foreach ($posales as $posale) {
            $sub += $posale['qt'];
        }
        echo $sub;
    }

    public function GetDiscount($id) {
        $CustomerModel = new Customer();
        $customer = $CustomerModel->find($id);
        $Discount = 0;
        echo $Discount . '~' . $customer['name'];
    }

    public function ResetPos() {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $PosaleModel = new Posale();
        $condition = array('status' => 1, 'register_id' => $this->register);
        $PosaleModel->where($condition)->delete();
        
        echo json_encode(array(
            "status" => TRUE
        ));
    }

//***************************
// Finalizar venta
//
//**************************
    public function AddNewSale($type) {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        
        $SettingModel = new Setting();
        $RegisterModel = new Register();
        $StoreModel = new Store();
        $SaleModel = new Sale();
        $PosaleModel = new Posale();
        $ProductModel = new Product();
        $Combo_itemModel = new Combo_item();
        $StockModel = new Stock();
        $Sale_itemModel = new Sale_item();
        $WaiterModel = new Waiter();
        $TableModel = new Table();
        $HoldModel = new Hold();
        $Stripe = new Stripe();
        $Charge = new Charge();
        
        $this->setting = $SettingModel->find(1);
        
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        $_POST['created_at'] = $date;
        $_POST['register_id'] = $this->register;
        $register = $RegisterModel->find($this->register);
        $store = $StoreModel->find($register['store_id']);
        if ($type == 2) {
            try {
                $setkey = $Stripe->setApiKey($this->setting['stripe_secret_key']);
               
                $myCard = array(
                    'number' => isset($_POST['ccnum']) ? $_POST['ccnum'] : NULL,
                    'exp_month' => isset($_POST['ccmonth']) ? $_POST['ccmonth'] : NULL,
                    'exp_year' => isset($_POST['ccyear']) ? $_POST['ccyear'] : NULL,
                    "cvc" => isset($_POST['ccv']) ? $_POST['ccv'] : NULL
                );
                $charge = $Charge->create(array(
                            'card' => $myCard,
                            'amount' => (floatval(isset($_POST['paid']) ? $_POST['paid'] : NULL) * 100),
                            'currency' => $this->setting['currency']
                ));
                
                echo "<p class='bg-success text-center'>" . label('saleStripesccess') . '</p>';
            } catch (ErrorObject $e) {
                // Since it's a decline, Stripe_CardError will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                echo "<p class='bg-danger text-center'>" . $err['message'] . '</p>';
            }
        }
        unset($_POST['ccnum']);
        unset($_POST['ccmonth']);
        unset($_POST['ccyear']);
        unset($_POST['ccv']);
        $paystatus = $_POST['paid'] - $_POST['total'];
        $_POST['firstpayement'] = $paystatus > 0 ? $_POST['total'] : $_POST['paid'];
        
        $saleId = $SaleModel->insert($_POST);
        $sale = $SaleModel->find($saleId);
        $posCond = array('status' => 1, 'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $posales = $PosaleModel->where($posCond)->find();
        
        foreach ($posales as $posale) {
            $data = array(
                "product_id" => $posale['product_id'],
                "name" => $posale['name'],
                "price" => $posale['price'],
                "qt" => $posale['qt'],
                "subtotal" => $posale['qt'] * $posale['price'],
                "sale_id" => $sale['id'],
                "date" => $date
            );
            $number = $posale['number'];
            $register = $RegisterModel->find($this->register);
            $prod = $ProductModel->find($posale['product_id']);
            if ($prod['type'] == "2") {
                /*                 * **************************************** combo case ************************************************************ */
                $combos = $Combo_itemModel->where('product_id',$posale['product_id'])->find();
                
                foreach ($combos as $combo) {
                    $prd = $ProductModel::find($combo['item_id']);
                    if ($prd['type'] == '0') {
                        $stockCond = array('store_id'=>$register['store_id'],'product_id' => $combo['item_id']);
                        $stock = $StockModel->where($stockCond)->first();
                        $stock['quantity'] = $stock['quantity'] - ($combo['quantity'] * $posale['qt']);
                        $StockModel->save($stock);
                    }
                }
                /*                 * **************************************************************************************************** */
            } else if ($prod['type'] == "0") {
                $stCond = array('store_id' =>$register['store_id'],'product_id' => $posale['product_id']);
                $stock = $StockModel->where($stCond)->first();
                $stock['quantity'] = $stock['quantity'] - $posale['qt'];
                $StockModel->save($stock);
            }
            $pos = $Sale_itemModel->insert($data);
        }
        $sale['created_at'] = date('d-m-Y H:i:s',strtotime($sale['created_at']));
        $WaiterName = $WaiterModel->select('name')->first($sale['waiter_id']);
        $WaiterName = $WaiterName['name'];
        $waiterN = $sale['waiter_id'] > 0 ? $WaiterName : label('withoutWaiter');
        $date = date("d-m-Y ") . "&nbsp;&nbsp;&nbsp;Hora:&nbsp;" . date("H:i");
        $tableN = $TableModel->select('name')->first($this->selectedTable);
        $tableN = $tableN['name'];
        //$address = Customer::find($sale->client_id)->email;
        //$phone = Customer::find($sale->client_id)->phone;

        $ticket = '<div padding-top:10px; padding-left:0px; ><div class="text-center">' . $this->setting['receiptheader'] . '</div>
        <div style="clear:both;"><h4 class="text-center">' . '##: ' . sprintf("%05d", $sale['id']) . '</h4> </div>
        <div style="clear:both;"><span style="text-align:left; font-weight:900; font-size:14px;">' . label("Date") . ': ' . $date . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Atiende: ' . $waiterN . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:900; font-size:17px;">Ref: ' . $tableN . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">' . label("Customer") . ': ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">RUT: ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Direccion: ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Tel: ' . "" . '</span></div>

        <div>
        <table class="table" cellspacing="0" border="0">
        <tr>
        <td style="text-align:center; border-bottom:1px solid #000; border-top:1px solid #000; font-weight:900; font-size:14px;">Cant</td>
        <td style="text-align:center; border-bottom:1px solid #000; border-top:1px solid #000; font-weight:900; font-size:14px;">' . label("Product") . '</td>
        
        <td style="text-align:right; border-bottom:1px solid #000; border-top:1px solid #000; font-weight:900; font-size:14px;">Importe</td>
        </tr>';

        $i = 1;
        foreach ($posales as $posale) {
            $options = "";
            if ($posale['options'] != '') {
                $options = "##" . $posale['options'];
            }
            $ticket .= '<tr>
             <td style="text-align:center; font-weight:900; width:40px;  font-size:15px;">' . $posale['qt'] . '</td>
            <td style="text-align:left; width:190px; font-weight: 900; font-size: 1.5em;">' . $posale['name'] . '<br>&nbsp;' . trim($posale['options'], ",") . '</td>
           
            <td style="text-align:right; width:80px; font-weight:900; font-size:15px; ">' . number_format((float) ($posale['qt'] * $posale['price'])) . ' ' . $this->setting['currency'] . '</td>
            </tr>';
            $i ++;
        }

        $bcs = 'code128';
        $height = 20;
        $width = 3;
        $iva = ($sale['subtotal'] - ($sale['subtotal'] / 1.22));
        $subT = ($sale['subtotal'] - $iva);
        $ticket .= '
        <tr>
        <td colspan="2" style="text-align:right;  border-top:1px solid #000; font-size:12px;">Sub Total: </td>
        <td style="text-align:right; border-top:1px solid #000; font-size:12px;">' . number_format((float) $subT, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td>
        </tr>';

        //***********************
        // Muestra importe IVA
        //***********************

        if (intval($sale['tax']))
            $ticket .= ' <tr>
            <td style="text-align:right;   font-size:12px;">' . label("tax") . '</td>
            <td style="text-align:center;  font-size:12px;">' . $sale['tax'] . '</td>
            <td style="text-align:right;  font-size:12px;">' . number_format((float) $iva, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . ' </td>
        </tr>
        <tr>
            <td style="text-align:left; font-weight:900; border-top:1px solid #000; font-weight:900; font-size:20px;">' . label("TotalItems") .': '. $sale['totalitems'] . '</td>
            <td style="text-align:left; font-weight:900; border-top:1px solid #000; font-weight:900; font-size:20px;">' . label("Total") . ': </td>
        <td style="text-align:right;font-weight:900; border-top:1px solid #000; font-weight:900; font-size:20px;">' . $sale['subtotal'] . ' ' . $this->setting['currency'] . '</td>
        </tr>';


        //***********************
        // Muestra forma de pago y vuelto
        //***********************

        $PayMethode = explode('~', $sale['paidmethod']);

        switch ($PayMethode[0]) {
            case '1': // case Credit Card
                $ticket .= '<td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">' . label("CreditCard") . '</td><td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;">xxxx xxxx xxxx ' . substr($PayMethode[1], - 4) . '</td></tr><tr>
                <td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">' . label("CreditCardHold") . '</td>
                <td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;">' . $PayMethode[2] . '</td></tr></tbody></table>';
                break;
            case '2': // case ckeck
                $ticket .= '<td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">' . label("ChequeNum") . '</td><td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;">' . $PayMethode[1] . '</td></tr></tbody></table>';
                break;
            default:
                $change = $sale['paid'] - $sale['subtotal'];
                $ticket .= '<tr>
        <td colspan="2" style="text-align:left; border-top:1px dashed #000; font-weight:900; padding-top:5px;">' . label("Paid") . ': ' . $sale['paid'] . ' ' . $this->setting['currency'] . '  </td>
        <td colspan="2" style="border-top:1px dashed #000; padding-top:5px; text-align:right; font-weight:bold;"></td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left; border-top:1px dashed #000; border-bottom:1px dashed #000; font-weight:bold; padding-top:5px;">' . label("Change") . ': ' . $change . ' ' . $this->setting['currency'] . '  </td>
        <td colspan="2" style="border-top:1px dashed #000; border-bottom:1px dashed #000;  padding-top:5px; text-align:right; font-weight:bold;"></td>
        </tr>
        </table></div>';
        }
        //***********************
        // Muestra pie del recibo
        //***********************
        $ticket .= '
         <div style="clear:both; margin-top:10px;"><center><img style="margin-top:0px" src="data:image/png;base64,' . $this->GenerateBarcode($sale['id'], $bcs, $height, $width) . '" alt="' . $sale['id'] . '" /></center>
         <p class="text-center" style="margin:0 auto;margin-top:10px;">' . $store['footer_text'] . '</p>
         <div class="text-center" style="background-color:#000;padding:5px;width:85%;color:#fff;margin:0 auto;border-radius:3px;margin-top:20px;">' . $this->setting['receiptfooter'] . '</div>';

        $posC = array('status' => 1, 'register_id' => $this->register, 'table_id' => rtrim($this->selectedTable, "h"));
        $PosaleModel->where($posC)->delete();
        
        if (isset($number)) {
            if ($number != 1)
                Hold::delete_all(array(
                    'conditions' => array(
                        'number = ? AND register_id = ? AND table_id = ?',
                        $number,
                        $this->register,
                        rtrim($this->selectedTable, "h")
                    )
                ));
        }
        $hConditions = array('register_id' => $this->register, 'table_id'=> $this->selectedTable);
        $hold = $HoldModel->where($hConditions)->orderBy('id', 'desc')->limit(1)->first();
       
        if ($hold) {
            $updateData = [
                'status' => 1
            ];
            $updateCondition = array('number' => $hold['number'], 'register_id' =>$this->register, 'table_id' =>  rtrim($this->selectedTable, "h"));
             $PosaleModel->where($updateCondition)->set(['status' => 1])->update();
//            $PosaleModel->where($updateCondition)->update($updateData);
            
        }
        echo $ticket;
    }

    function GenerateBarcode($code = NULL, $bcs = 'code128', $height = 60, $width = 1) {
        $barcode = new Code128([
            'text' => $code,
            'barHeight' => $height,
            'factor' => 2,
            'drawText' => FALSE
        ]);

        $renderer = new Image([
            'resource' => imagecreate($barcode->getWidth(), $barcode->getHeight()),
            'barcode' => $barcode,
        ]);
        
        ob_start();
        $renderer->render();
        $image = base64_encode(ob_get_contents());
        ob_end_clean();
    
        return $image;
    }

    // ******************************************************** hold functions
    public function holdList($registerid) {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        
        $HoldModel = new Hold();
        $PosaleModel = new Posale();
        
        $holdConditions = array('register_id' => $registerid, 'table_id' => $this->selectedTable);
        $holds = $HoldModel->where($holdConditions)->orderBy('number', 'asc')->find();
        
        $posaleConditions = array('status' => 1,'register_id' =>  $this->register,'table_id' => $this->selectedTable);
        $posale = $PosaleModel->where($posaleConditions)->orderBy('id', 'desc')->limit(1)->first();
        
        $Tholds = '';
        if (empty($holds))
            $Tholds = '<span class="Hold selectedHold">1<span id="Time">' . date("H:i") . '</span></span>';
        else {
            if (empty($posale)) {
                $numItems = count($holds);
                $i = 0;
                foreach ($holds as $hold) {
                    if (++$i === $numItems)
                        $Tholds .= '<span class="Hold selectedHold" id="' . $hold['number'] . '"  onclick="SelectHold(' . $hold['number'] . ')">' . $hold['number'] . '<span id="Time">' . $hold['time'] . '</span></span>';
                    else
                        $Tholds .= '<span class="Hold" id="' . $hold['number'] . '"  onclick="SelectHold(' . $hold['number'] . ')">' . $hold['number'] . '<span id="Time">' . $hold['time'] . '</span></span>';
                }
            } else {
                foreach ($holds as $hold) {
                    if ($hold['number'] == $posale['number'])
                        $selected = 'selectedHold';
                    else
                        $selected = '';
                    $Tholds .= '<span class="Hold ' . $selected . '" id="' . $hold['number'] . '" onclick="SelectHold(' . $hold['number'] . ')">' . $hold['number'] . '<span id="Time">' . $hold['time'] . '</span></span>';
                }
            }
        }
        echo $Tholds;
    }

    public function AddHold($registerid) {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        
        $HoldModel = new Hold();
        $PosaleModel = new Posale();
        
        $holdConditions = array('register_id' =>  $registerid,'table_id' => $this->selectedTable);
        $hold = $HoldModel->where($holdConditions)->orderBy('id', 'desc')->limit(1)->first();
        $number = !empty($hold) ? intval($hold['number']) + 1 : 1;
       
        $posaleCondition = array('status' => 1, 'register_id' => $this->register);
       
        $PosaleModel->where($posaleCondition)->set(['status' => 0])->update();
        $attributes = array(
            'number' => $number,
            'time' => date("H:i"),
            'register_id' => $registerid,
            'table_id' => $this->selectedTable
        );
        $HoldModel->insert($attributes);
        echo json_encode(array(
            "status" => TRUE
        ));
    }

    public function RemoveHold($number, $registerid) {
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        
        $HoldModel = new Hold();
        $PosaleModel = new Posale();
        
        $holdConditions = array('number' => $number, 'register_id' => $registerid, 'table_id' => $this->selectedTable);
        $hold = $HoldModel->where($holdConditions)->first();
        $HoldModel->delete($hold['id']);
        
        $condition = array('number' => $number, 'register_id' => $registerid);
        $PosaleModel->where($condition)->delete();
        
        $Holdcond = array('register_id' =>  $registerid,'table_id' => $this->selectedTable);
        $hold = $HoldModel->where($Holdcond)->orderBy('id', 'desc')->limit(1)->first();
       
        
        $conditions = array('number' => $hold['number'], 'register_id' => $registerid);
        
        $PosaleModel->where($conditions)->set(['status' => 1])->update();
        echo json_encode(array(
            "status" => TRUE
        ));
    }

    public function SelectHold($number) {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $PosaleModel = new Posale();
       
        $firstCondition = array('status' => 1, 'register_id' => $this->register);
       
        $PosaleModel->where($firstCondition)->set(['status' => 0])->update();
        
        $secondCondition = array('number' => $number, 'register_id' => $this->register);
        $secondData = [
            'status' => 1
        ];
       
        $PosaleModel->where($secondCondition)->set(['status' => 1])->update();
        echo json_encode(array(
            "status" => TRUE
        ));
    }

    /**
     * ****************** register functions ***************
     */
    public function CloseRegister() {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        
        $RegisterModel = new Register();
        $UserModel = new User();
        $SaleModel = new Sale();
        $PayementModel = new Payement();
        $WaiterModel = new Waiter();
        $SettingModel = new Setting();
        
        $this->setting = $SettingModel->find(1);
        $register = $RegisterModel->find($this->register);
        $user = $UserModel->find($register->user_id);
        $sales = $SaleModel->where('register_id',$this->register)->find();
        $payaments = $PayementModel->where('register_id',$this->register)->find();
        $waiters = $WaiterModel->where('store_id',$register->store_id)->find();

        $cash = 0;
        $cheque = 0;
        $cc = 0;
        $CashinHand = $register['cash_inhand'];
        $date = $register['date'];
        $createdBy = $user['firstname'] . ' ' . $user['lastname'];

        foreach ($payaments as $payament) {
            $PayMethode = explode('~', $payament['paidmethod']);
            switch ($PayMethode[0]) {
                case '1': // case Credit Card
                    $cc += $payament['paid'];
                    break;
                case '2': // case ckeck
                    $cheque += $payament['paid'];
                    break;
                default:
                    $cash += $payament['paid'];
            }
        }

        foreach ($sales as $sale) {
            $PayMethode = explode('~', $sale['paidmethod']);
            $paystatus = $sale['paid'] - $sale['total'];
            switch ($PayMethode[0]) {
                case '1': // case Credit Card
                    $cc += $paystatus > 0 ? $sale['total'] : $sale['firstpayement'];
                    break;
                case '2': // case ckeck
                    $cheque += $paystatus > 0 ? $sale['total'] : $sale['firstpayement'];
                    break;
                default:
                    $cash += $paystatus > 0 ? $sale['total'] : $sale['firstpayement'];
            }
        }
        $data = '<div class="col-md-3"><blockquote><footer>' . label("Openedby") . '</footer><p>' . $createdBy . '</p></blockquote></div><div class="col-md-3"><blockquote><footer>' . label("CashinHand") . '</footer><p>' . number_format((float) $CashinHand, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</p></blockquote></div><div class="col-md-4"><blockquote><footer>' . label("Openingtime") . '</footer><p>' . $date->format('Y-m-d h:i:s') . '</p></blockquote></div><div class="col-md-2"><img src="' . site_url() . '/assets/img/register.svg" alt=""></div><h2>' . label("PaymentsSummary") . '</h2><table class="table table-striped"><tr><th width="25%">' . label("PayementType") . '</th><th width="25%">' . label("EXPECTED") . ' (' . $this->setting['currency'] . ')</th><th width="25%">' . label("COUNTED") . ' (' . $this->setting['currency'] . ')</th><th width="25%">' . label("DIFFERENCES") . ' (' . $this->setting['currency'] . ')</th></tr><tr><td>' . label("Cash") . '</td><td><span id="expectedcash">' . number_format((float) $cash, $this->setting['decimals'], '.', '') . '</span></td><td><input type="text" class="total-input" value="' . number_format((float) $cash, $this->setting['decimals'], '.', '') . '" placeholder="0.00"  maxlength="11" id="countedcash"></td><td><span id="diffcash">0.00</span></td></tr><tr><td>' . label("CreditCard") . '</td><td><span id="expectedcc">' . number_format((float) $cc, $this->setting['decimals'], '.', '') . '</span></td><td><input type="text" class="total-input" value="' . number_format((float) $cc, $this->setting['decimals'], '.', '') . '" placeholder="0.00"  maxlength="11" id="countedcc"></td><td><span id="diffcc">0.00</span></td></tr><tr><td>' . label("Cheque") . '</td><td><span id="expectedcheque">' . number_format((float) $cheque, $this->setting['decimals'], '.', '') . '</span></td><td><input type="text" class="total-input" value="' . number_format((float) $cheque, $this->setting['decimals'], '.', '') . '" placeholder="0.00"  maxlength="11" id="countedcheque"></td><td><span id="diffcheque">0.00</span></td></tr><tr class="warning"><td>' . label("Total") . '</td><td><span id="total">' . number_format((float) ($cheque + $cash + $cc), $this->setting['decimals'], '.', '') . '</span></td><td><span id="countedtotal">' . number_format((float) ($cheque + $cash + $cc), $this->setting['decimals'], '.', '') . '</span></td><td><span id="difftotal">0.00</span></td></tr></table>';

        foreach ($waiters as $waiter) {
            $cih = explode(',', trim($register['waiterscih'], ","));
            $cachin = 0;
            for ($i = 0; $i < sizeof($cih); $i += 2) {
                if ($cih[$i] == $waiter['id']) {
                    $cachin = $cih[$i + 1];
                }
            }
            $cashw = 0;
            $chequew = 0;
            $ccw = 0;
            foreach ($payaments as $payament) {
                if ($payament['waiter_id'] == $waiter['id']) {
                    $PayMethode = explode('~', $payament['paidmethod']);
                    switch ($PayMethode[0]) {
                        case '1': // case Credit Card
                            $ccw += $payament['paid'];
                            break;
                        case '2': // case ckeck
                            $chequew += $payament['paid'];
                            break;
                        default:
                            $cashw += $payament['paid'];
                    }
                }
            }
            foreach ($sales as $sale) {
                if ($sale['waiter_id'] == $waiter['id']) {
                    $PayMethode = explode('~', $sale['paidmethod']);
                    $paystatus = $sale['paid'] - $sale['total'];
                    switch ($PayMethode[0]) {
                        case '1': // case Credit Card
                            $ccw += $paystatus > 0 ? $sale['total'] : $sale['firstpayement'];
                            break;
                        case '2': // case ckeck
                            $chequew += $paystatus > 0 ? $sale['total'] : $sale['firstpayement'];
                            break;
                        default:
                            $cashw += $paystatus > 0 ? $sale['total'] : $sale['firstpayement'];
                    }
                }
            }
            $Wtotal = $ccw + $chequew + $cashw + $cachin;
            $data .= '<div class="waitercount"><ul><li><h4>' . $sale['waiter_id'] . ' :</h4></li><li><b>' . label("CashinHand") . ' : </b>' . number_format((float) $cachin, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</li><li><b>' . label("Cash") . ' : </b>' . number_format((float) $cashw, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</li><li><b>' . label("CreditCard") . ' : </b>' . number_format((float) $ccw, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</li><li><b>' . label("Cheque") . ' : </b>' . number_format((float) $chequew, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</li></ul><div style="clear:both;"></div><div class="wtotal"><h3>' . label("Total") . ' : ' . number_format((float) $Wtotal, $this->setting['decimals'], '.', '') . '</h3></div></div>';
        }

        $data .= '<div  class="form-group"><h2>' . label("note") . '</h2><textarea id="RegisterNote" class="form-control" rows="3"></textarea></div>';

        echo $data;
    }

    public function SubmitRegister() {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $SettingModel = new Setting();
        $RegisterModel = new Register();
        $StoreModel = new Store();
        $TableModel = new Table();
        $HoldModel = new Hold();
        $PosaleModel = new Posale();
       
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        $data = array(
            "cash_total" => isset($_POST['expectedcash']) ? $_POST['expectedcash'] : NULL,
            "cash_sub" => isset($_POST['countedcash']) ? $_POST['countedcash'] : NULL,
            "cc_total" => isset($_POST['expectedcc']) ? $_POST['expectedcc'] : NULL,
            "cc_sub" => isset($_POST['countedcc']) ? $_POST['countedcc'] : NULL,
            "cheque_total" => isset($_POST['expectedcheque']) ? $_POST['expectedcheque'] : NULL,
            "cheque_sub" => isset($_POST['countedcheque']) ? $_POST['countedcheque'] : NULL,
            "note" => isset($_POST['RegisterNote']) ? $_POST['RegisterNote'] : NULL,
            "closed_by" => $this->session->userdata('user_id'),
            "closed_at" => $date,
            "status" => 0
        );

        $Register = $RegisterModel->find($this->register);
        $store = $StoreModel->find($Register->store_id);
        $store['status'] = 0;
        $StoreModel->save($store);

        $tables = $TableModel->where('store_id',$Register['store_id'])->find();
        
        foreach ($tables as $table) {
            $table->status = 0;
            $table->time = '';
            $table->save();
        }

        $RegisterModel->update($this->register, $data);

        $HoldModel->where('register_id', $Register['id'])->delete();
       
        $PosaleModel->where('register_id', $Register['id'])->delete();
       
        $this->session->set('register', 0);

        echo json_encode(array(
            "status" => TRUE
        ));
    }

    public function email() {
        $email_id = isset($_POST['email']) ? $_POST['email'] : NULL;
        $content = isset($_POST['content']) ? $_POST['content'] : NULL;
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        $email = \Config\Services::email();
//        $email->setFrom('no-reply@' . $this->setting['companyname'] . '.com', $this->setting['companyname']);
        $email->setFrom('no-reply@sistema.com', $this->setting['companyname']);
        $email->setTo($email_id);
        $email->setSubject('your Receipt');
        $email->setMessage($content);
        $email->send();
        echo json_encode(array(
            "status" => TRUE
        ));
    }

    public function pdfreceipt() {
        $content = isset($_POST['pdf_content']) ? $_POST['pdf_content'] : NULL;
        $dompdf = new Dompdf();
        $dompdf->loadHtml($content);
        $dompdf->set_paper(array(0,0,350,800));
        $dompdf->render();
        $dompdf->output();
        $dompdf->stream('Brochure.pdf');
        exit;
    }

    public function storewaitercash($id) {
        $WaiterModel = new Waiter();
        $waiters = $WaiterModel->where('store_id',$id)->find();
        $content = '';
        foreach ($waiters as $waiter) {
            $content .= '<div class="form-group"><label for="CashinHand"><u>'.$waiter['name'].'</u> '.label("CashinHand").'</label><input type="number" step="any" class="form-control" id="waiterid" waiter-id="'.$waiter['id'].'" placeholder="'.$waiter['name'].' '.label("CashinHand").'" Required></div>';
        }
        echo $content;
    }

    public function WaiterName($num = null) {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        $HoldModel = new Hold(); 
        $HoldCondition = array('number' => $num, 'register_id' => $this->register , 'table_id' => $this->selectedTable);
        $waiterid = $HoldModel->select('waiter_id')->where($HoldCondition)->first();
       
        echo $waiterid['waiter_id'];
    }

    public function changewaiterS() {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
       
        $num = isset($_POST['num']) ? $_POST['num'] : NULL;
        $id = isset($_POST['id']) ? $_POST['id'] : NULL;
//        $num = $this->input->post('num');
//        $id = $this->input->post('id');
        $HoldModel = new Hold(); 
        $holdCondition = array('number' => $num, 'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $hold = $HoldModel->where($holdCondition)->first();
       
        $hold['waiter_id'] = $id;
        $HoldModel->save($hold);

        echo json_encode(array(
            "status" => TRUE
        ));
    }

    public function CustomerName($num = null) {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        $HoldModel = new Hold(); 
        $holdCondition = array('number' => $num, 'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $customerid = $HoldModel->select('customer_id')->where($holdCondition)->first();
       
        echo $customerid['customer_id'];
    }

    public function changecustomerS() {
        $num = '';
        if ($this->request->isAJAX()) {
            $num = service('request')->getPost('num');
           
            var_dump($this->request->getPost('num'));
        }
       
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
//        $num = $this->input->post('num');
//        $id = $this->input->post('id');
        $num = isset($_POST['num']) ? $_POST['num'] : NULL;
        $id = isset($_POST['id']) ? $_POST['id'] : NULL;
        
        
        $HoldModel = new Hold(); 
        $holdCondition = array('number' => $num, 'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $hold = $HoldModel->where($holdCondition)->first();
       
        $hold['customer_id'] = $id;
        $HoldModel->save($hold);

        echo json_encode(array(
            "status" => TRUE
        ));
    }

    //*********************************
    //
   //IMPRIMIR COMANDA
    //
   //****************************

    public function showticket($num, $subtotal, $totalitems, $waiter, $clientID) {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        $this->store = $this->session->get('store') ? $this->session->get('store') : FALSE;
        
        $SettingModel = new Setting();
        $WaiterModel = new Waiter();
        $StoreModel = new Store();
        $TableModel = new Table();
        $PosaleModel = new Posale();
       
        $this->setting = $SettingModel->find(1);
        // $hold = Hold::find($num);
        $waiterName = $WaiterModel->select('name')->first($waiter);
        $waiterN = $waiter > 0 ? $waiterName['name'] : label('withoutWaiter');
        $customerN = $clientID;
        $store = $StoreModel->find($this->store);
        $date = date("d-m-Y ") . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hora:&nbsp;" . date("H:i");
        $tableN = $TableModel->select('name')->first($this->selectedTable);
        $tableN = $tableN['name'];
        $total = 0;
        $posCond = array('status' => 1, 'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $posales = $PosaleModel->where($posCond)->find();

        $ticket = '<div ><div class="text-center">' . $this->setting['receiptheader'] . '</div><div style="clear:both;"><div style="clear:both;">
                    <div style="clear:both;"><span style="text-align:left; font-weight:900; font-size:14px;">' . label("Date") . ': ' . $date . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Atiende: ' . $waiterN . '</span></div>
            <span style="font-weight: 800; font-size: 18px;">Ref:&nbsp' . $tableN . '</span><br>
            <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">' . label("Customer") . ': ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">RUT: ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Direccion: ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Tel: ' . "" . '</span></div>
            </div>
            
            <table class="table" cellspacing="0" border="0">
            <tr>
            <td style="text-align:left; border-bottom:1px solid #000; border-top:1px solid #000;" ><h4>' . label("Product") . '</h4></td>
            <td style="text-align:center; border-bottom:1px solid #000; border-top:1px solid #000;"><h4>Cant</h4></td>
            <td style="text-align:right; border-bottom:1px solid #000; border-top:1px solid #000;"><h4>Importe</h4></td>
            </tr>';

        $i = 1;
        foreach ($posales as $posale) {
            $ticket .= '<tr>
           <td style="text-align:left; width:190px; font-weight: 900; font-size: 1.5em;">' . $posale['name'] . '<br>' . $posale['options'] . '</td>
           <td style="text-align:center; width:50px; font-weight: 900; font-size: 1.5em;">' . $posale['qt'] . '</td>
           <td style="text-align:right; width:100px; font-size: 1.5em;">' . ($posale['qt'] * $posale['price']) . ' ' . $this->setting['currency'] . '</td>
           </tr>
        
           ';
            $total = $total + ($posale['qt'] * $posale['price']);

            $i ++;
        }

        $ticket .= '
	   <tr>
             
      <tr>
	   <tr>
             <td style="text-align:left; width:190px; border-top:1px solid #000; font-weight: 900; font-size: 18px;">' . label("TotalItems") . ': &nbsp' . $totalitems . '</td>
             <td style="text-align:center; width:50px; border-top:1px solid #000; font-weight: 900; font-size: 1.8em;">' . label("Total") . ':</td>
             <td style="text-align:right; width:100px; border-top:1px solid #000; font-weight: 900; font-size: 1.8em;">' . $total . ' ' . $this->setting['currency'] . '</td>
             
          </tr>    
        <tr>    
             
        <td colspan="2" style="text-align:left; font-weight:900; padding-top:5px;"> </td>
        
          </tr>    
        <tr> 
        <td colspan="2" style="text-align:left; border-top:1px dashed #000; font-weight:900; border-bottom:1px dashed #000; font-weight:bold; padding-top:5px;">' . label("Paid") . ': ' . $this->setting['currency'] . '  </td>
        <td colspan="2" style="border-top:1px dashed #000; border-bottom:1px dashed #000;  padding-top:5px; text-align:right; font-weight:bold;"></td>
        </tr>
        </table></div>';



        $ticket .= '
      <div style="clear:both;"><p class="text-center" style="margin:0 auto;margin-top:10px;">' . $store['footer_text'] . '</p>
      <div class="text-center" style="background-color:#000;padding:5px;width:85%;color:#fff;margin:0 auto;border-radius:3px;margin-top:20px;">' . $this->setting['receiptfooter'] . '</div>
      </div>';

        echo $ticket;
    }

    public function showticketKit($tableid) {
        $TableModel = new Table();
        $PosaleModel = new Posale();
        $table = $TableModel->find($tableid);
        $tableN = $table['name'];
        $posales = $PosaleModel->where('table_id',$tableid)->find();
        
        foreach ($posales as $posale) {
            $d1 = new DateTime($posale['time']);
            $d2 = new DateTime($table['checked']);
            if ($d1 < $d2) {
                $posale['time'] = 'y';
            } else {
                $posale['time'] = 'n';
            }
        }
        $table['checked'] = date("Y-m-d H:i:s");
        $TableModel->save($table);



        $ticket = '<div class="col-md-12"><div class="text-center">' . $this->setting['receiptheader'] . '</div><div style="clear:both;"><br><div style="clear:both;"><div style="clear:both;"><br><div style="clear:both;">' . label("Table") . ' :' . $tableN . '</span><div style="clear:both;"><br><br><table class="table" cellspacing="0" border="0"><thead><tr><th><em>#</em></th><th>' . label("Product") . '</th><th>' . label("Quantity") . '</th><th>' . label("SubTotal") . '</th></tr></thead><tbody>';

        $i = 1;
        foreach ($posales as $posale) {
            $ticket .= '<tr style="' . ($posale['time'] == "n" ? 'background-color:#FFC0CB;' : '') . '"><td style="text-align:center; width:30px;"></td><td style="text-align:left; width:180px;">' . $posale['name'] . '<br><span style="font-size:12px;color:#666">' . $posale['options'] . '</span></td><td style="text-align:center; width:50px;">' . $posale['qt'] . '</td><td style="text-align:right; width:70px;font-size:14px; ">' . number_format((float) ($posale['qt'] * $posale['price']), $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td></tr>';
            $i ++;
        }

        $ticket .= '</tbody></table>';


        echo $ticket;
    }

    public function getoptions($id, $posale) {
        $ProductModel = new Product();
        $PosaleModel = new Posale();
        $options = $ProductModel->select('options')->first($id);
        $options = $options['options'];
        $options = trim($options, ",");
        $array = explode(',', $options); //split string into array seperated by ','
        $poOptions = $PosaleModel->select('options')->first($posale);
        $poOptions = $poOptions['options'];
        $poOptions = trim($poOptions, ",");
        $array2 = explode(',', $poOptions); //split string into array seperated by ','
        $result = '<div class="col-md-12"><input type="hidden" value="' . $posale . '" id="optprd"><select class="js-select-basic-multiple form-control" multiple="multiple" id="optionsselect">';
        foreach ($array as $value) {
            $selected = '';
            foreach ($array2 as $value2) {
                $selected = $value == $value2 ? 'selected="selected"' : $selected;
            }
            $result .= '<option value="' . $value . '" ' . $selected . '>' . $value . '</option>';
        }
        $result .= '</select></div>';
        echo $result;
    }

    public function addposaleoptions() {
        $PosaleModel = new Posale();
        $options = isset($_POST['options']) ? $_POST['options'] : NULL;
        $posaleid = isset($_POST['posale']) ? $_POST['posale'] : NULL;
        $option = '';
        foreach ($options as $value) {
            $option .= $value . ',';
        }
        $posale = $PosaleModel->find($posaleid);
        $posale['options'] = $option;
        $posale['time'] = date("Y-m-d H:i:s");
        $PosaleModel->save($posale);

        echo json_encode(array(
            "status" => TRUE
        ));
    }

    public function CloseTable() {
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        $HoldModel = new Hold();
        $PosaleModel = new Posale();
        $TableModel = new Table();
        
        $holdCondition = array('table_id' => intval($this->selectedTable), 'register_id' => $this->register);
        $HoldModel->where($holdCondition)->delete();
        
        $posaleCondition = array('table_id' => intval($this->selectedTable), 'register_id' => $this->register);
        $PosaleModel->where($posaleCondition)->delete();

        if ($this->selectedTable != '0h') {

            $table = $TableModel->find($this->selectedTable);
            $table['status'] = 0;
            $table['time'] = '';
            $TableModel->save($table);
        }

        $this->session->set('selectedTable', 0);

        echo json_encode(array(
            "status" => TRUE
        ));
    }

//********************************
// Finalizar venta cobro pendiente
//
//********************************
    public function AddNewSale2($type) {
        $RegisterModel = new Register();
        $StoreModel = new Store();
        $SettingModel = new Setting();
        $SaleModel = new Sale();
        $PosaleModel = new Posale();
        $ProductModel = new Product();
        $Combo_itemModel = new Combo_item();
        $StockModel = new Stock();
        $TableModel = new Table();
        $HoldModel = new Hold();
        $WaiterModel = new Waiter();
        $Stripe = new Stripe();
        $Charge = new Charge();
        
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->selectedTable = $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        $this->setting = $SettingModel->find(1);
        
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        $_POST['created_at'] = $date;
        $_POST['register_id'] = $this->register;
        $register = $RegisterModel->find($this->register);
        $store = $StoreModel->find($register['store_id']);
        if ($type == 2) {
            try {
                $Stripe->setApiKey($this->setting['stripe_secret_key']);
                $myCard = array(
                    'number' => isset($_POST['ccnum']) ? $_POST['ccnum'] : NULL,
                    'exp_month' => isset($_POST['ccmonth']) ? $_POST['ccmonth'] : NULL,
                    'exp_year' => isset($_POST['ccyear']) ? $_POST['ccyear'] : NULL,
                    "cvc" => isset($_POST['ccv']) ? $_POST['ccv'] : NULL
                );
                $charge = $Charge->create(array(
                            'card' => $myCard,
                            'amount' => (floatval(isset($_POST['paid']) ? $_POST['paid'] : NULL) * 100),
                            'currency' => $this->setting['currency']
                ));
                echo "<p class='bg-success text-center'>" . label('saleStripesccess') . '</p>';
            } catch (ErrorObject $e) {
                // Since it's a decline, Stripe_CardError will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                echo "<p class='bg-danger text-center'>" . $err['message'] . '</p>';
            }
        }
        unset($_POST['ccnum']);
        unset($_POST['ccmonth']);
        unset($_POST['ccyear']);
        unset($_POST['ccv']);
        $paystatus = $_POST['paid'] - $_POST['total'];
        $_POST['firstpayement'] = $paystatus > 0 ? $_POST['total'] : $_POST['paid'];
        
        $saleId = $SaleModel->insert($_POST);
        $sale = $SaleModel->find($saleId);
        
        $PosaleConditions = array('status'=> 1, 'register_id' => $this->register, 'table_id' => $this->selectedTable);
        $posales = $PosaleModel->where($PosaleConditions)->find();
        
        foreach ($posales as $posale) {
            $data = array(
                "product_id" => $posale['product_id'],
                "name" => $posale['name'],
                "price" => $posale['price'],
                "qt" => $posale['qt'],
                "subtotal" => $posale['qt'] * $posale['price'],
                "sale_id" => $sale['id'],
                "date" => $date
            );
            $number = $posale['number'];
            $register = $RegisterModel->find($this->register);
            $prod = $ProductModel->find($posale['product_id']);
            if ($prod['type'] == "2") {
                /*                 * **************************************** combo case ************************************************************ */
                $combos = $Combo_itemModel->where('product_id',$posale['product_id'])->find();
                
                foreach ($combos as $combo) {
                    $prd = $ProductModel->find($combo['item_id']);
                    if ($prd['type'] == '0') {
                        $stockCond = array('store_id' => $register['store_id'], 'product_id' => $combo['item_id']);
                        $stock = $StockModel->where($stockCond)->first();
                        $stock['quantity'] = $stock['quantity'] - ($combo['quantity'] * $posale['qt']);
                        $StockModel->save($stock);
                    }
                }
                /*                 * **************************************************************************************************** */
            } else if ($prod['type'] == "0") {
                $stockCond1 = array('store_id' => $register['store_id'], 'product_id' => $posale['product_id']);
                $stock = $StockModel->where($stockCond1)->first();
                $stock['quantity'] = $stock['quantity'] - $posale['qt'];
                $StockModel->save($stock);
            }
            $pos = $SaleModel->insert($data);
        }
      
        $sale['created_at'] = date('d-m-Y H:i:s',strtotime($sale['created_at']));
        $waiterName = $WaiterModel->select('name')->first($sale['waiter_id']);
        $waiterName = $waiterName['name'];
        $waiterN = $sale['waiter_id'] > 0 ? $waiterName : label('withoutWaiter');
        $date = date("d-m-Y ") . "&nbsp;&nbsp;&nbsp;Hora:&nbsp;" . date("H:i");
        $tableN = $TableModel->select('name')->first($this->selectedTable);
        $tableN = $tableN['name'];
        //$address = Customer::find($sale->client_id)->email;
        //$phone = Customer::find($sale->client_id)->phone;

        $ticket = '<div padding-top:10px; padding-left:0px; ><div class="text-center">' . $this->setting['receiptheader'] . '</div>
        <div style="clear:both;"><h4 class="text-center">' . '##: ' . sprintf("%05d", $sale['id']) . '</h4> </div>
        <div style="clear:both;"><span style="text-align:left; font-weight:900; font-size:14px;">' . label("Date") . ': ' . $date . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Atiende: ' . $waiterN . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:900; font-size:17px;">Ref: ' . $tableN . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">' . label("Customer") . ': ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">RUT: ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Direccion: ' . "" . '</span></div>
        <div style="clear:both;"><span style="text-align:left; font-weight:500; font-size:12px;">Tel: ' . "" . '</span></div>

        <div>
        <table class="table" cellspacing="0" border="0">
        <tr>
        <td style="text-align:center; border-bottom:4px solid #000; border-top:4px solid #000; font-weight:900; font-size:14px;"></td>
        <td style="text-align:left; border-bottom:4px solid #000; border-top:4px solid #000; font-weight:900; font-size:14px;">' . label("Product") . '</td>
        <td style="text-align:right; border-bottom:4px solid #000; border-top:4px solid #000; font-weight:900; font-size:14px;">Importe</td>
        </tr>';

        $i = 1;
        foreach ($posales as $posale) {
            $options = "";
            if ($posale['options'] != '') {
                $options = "##" . $posale['options'];
            }
            $ticket .= '<tr>
             <td style="text-align:center; font-weight:900; width:40px;  font-size:15px;">' . $posale['qt'] . '</td>
            <td style="text-align:left; width:190px; font-weight: 900; font-size: 1.5em;">' . $posale['name'] . '<br>&nbsp;' . trim($posale['options'], ",") . '</td>
                <td style="text-align:right; width:80px; font-weight:900; font-size:15px; ">' . number_format((float) ($posale['qt'] * $posale['price'])) . ' ' . $this->setting['currency'] . '</td>
            </tr>';
            $i ++;
        }

        $bcs = 'code128';
        $height = 20;
        $width = 3;
        $iva = ($sale['subtotal'] - ($sale['subtotal'] / 1.22));
        $subT = ($sale['subtotal'] - $iva);
        $ticket .= '
        <tr>
        <td colspan="2" style="text-align:right;  border-top:4px solid #000; font-size:12px;">Sub Total: </td>
        <td style="text-align:right; border-top:4px solid #000; font-size:12px;">' . number_format((float) $subT, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td>
        </tr>';

        //***********************
        // Muestra importe IVA
        //***********************

        if (intval($sale['tax']))
            $ticket .= ' <tr>
            <td style="text-align:right;   font-size:12px;"></td>
            <td style="text-align:right;  font-size:12px;">' . label("tax") . '&nbsp;' . $sale['tax'] . '</td>
            <td style="text-align:right;  font-size:12px;">' . number_format((float) $iva, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . ' </td>
            </tr>
            <tr>
        <td style="text-align:rig; font-weight:900; border-top:4px solid #000; font-weight:900; font-size:20px;"></td>
        <td style="text-align:right; font-weight:900; border-top:4px solid #000; font-weight:900; font-size:20px;">' . label("Total") . ': </td>
        <td style="text-align:right;font-weight:900; border-top:4px solid #000; font-weight:900; font-size:20px;">' . $sale['subtotal'] . ' ' . $this->setting['currency'] . '</td>
        </tr>
        <tr>
        <td style="text-align:center; font-weight:900; border-top:4px solid #000; font-weight:900; font-size:20px;">' . $sale['totalitems'] . '</td>
        <td style="text-align:left; font-weight:900; border-top:4px solid #000; font-weight:900; font-size:20px;">items </td>
        <td style="text-align:right;font-weight:900; border-top:4px solid #000; font-weight:900; font-size:20px;"></td>
        </tr>';


        //***********************
        // Muestra forma de pago y vuelto
        //***********************

        $PayMethode = explode('~', $sale['paidmethod']);

        switch ($PayMethode[0]) {
            case '1': // case Credit Card
                $ticket .= '<td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">' . label("CreditCard") . '</td><td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;">xxxx xxxx xxxx ' . substr($PayMethode[1], - 4) . '</td></tr><tr>
                <td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">' . label("CreditCardHold") . '</td>
                <td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;">' . $PayMethode[2] . '</td></tr></tbody></table>';
                break;
            case '2': // case ckeck
                $ticket .= '<td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">' . label("ChequeNum") . '</td><td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;">' . $PayMethode[1] . '</td></tr></tbody></table>';
                break;
            default:
                $change = $sale['paid'] - $sale['subtotal'];
                $ticket .= '<tr>
        
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;  font-weight:900; padding-top:15px;">PAGA CON:  </td>
        <td colspan="2" style= padding-top:15px; text-align:right; font-weight:bold;"></td>
        </tr>
        </table></div>';
        }
        //***********************
        // Muestra pie del recibo
        //***********************
        $ticket .= '
         <div style="clear:both;">
         <p class="text-center" style="margin:0 auto;margin-top:10px;">' . $store['footer_text'] . '</p>
         <div class="text-center" style="background-color:#000;padding:5px;width:85%;color:#fff;margin:0 auto;border-radius:3px;margin-top:20px;">' . $this->setting['receiptfooter'] . '</div>'

        ;

        $posDeleteCond = array('status' => 1,'register_id' => $this->register, 'table_id' => rtrim($this->selectedTable, "h"));
        $PosaleModel->where($posDeleteCond)->delete();
        
        if (isset($number)) {
            if ($number != 1)
                $holdDeleteCond = array('number' => $number,'register_id' => $this->register, 'table_id' => rtrim($this->selectedTable, "h"));
                
        }
        $hConditions = array('register_id' => $this->register, 'table_id'=> $this->selectedTable);
        $hold = $HoldModel->where($hConditions)->orderBy('id', 'desc')->limit(1)->first();
        
        if ($hold) {
            $posUpdatedata = [
                'status' => 1
            ];
            $conditions = array('number' => $hold['number'], 'register_id' => $this->register, 'table_id' =>rtrim($this->selectedTable, "h"));
            
            $PosaleModel->where($conditions)->set(['status' => 1])->update();
            
        }
        echo $ticket;
        //' . facturar($sale->id, $ticket) . '
    }

}
