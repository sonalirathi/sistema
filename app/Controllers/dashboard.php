<?php
namespace App\Controllers;
use App\Models\Setting;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Category;
use App\Models\Store;
use App\Models\Posale;
use App\Models\Table;
use App\Models\Zone;
use App\Models\Waiter;
use App\Models\Register;
use App\Models\Stock;
use App\Models\Hold;

class Dashboard extends BaseController
{
    protected $session;
    
    function __construct()
    {
//        $this->register = $this->session->userdata('register') ? $this->session->userdata('register') : FALSE;
//        $this->store = $this->session->userdata('store') ? $this->session->userdata('store') : FALSE;
//        $this->selectedTable = $this->session->userdata('selectedTable') ? $this->session->userdata('selectedTable') : FALSE;
    }
    
    public function index()
    {
       if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        
        $data = array();
        $this->register =  $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->store =  $this->session->get('store') ? $this->session->get('store') : FALSE;
        $this->selectedTable =  $this->session->get('selectedTable') ? $this->session->get('selectedTable') : FALSE;
        $data['store'] = $this->store;
        $SettingModel = new Setting();
        $this->setting =  $SettingModel->find(1);
        $data['setting'] = $this->setting;
        $data['register'] = $this->register;
        $data['selectedTable'] = $this->selectedTable;
        $ProductModel = new Product();
        $HoldModel = new Hold();
        $user = array();
        $user['firstname'] = $this->session->get('firstname');
        $user['lastname'] = $this->session->get('lastname');
        $data['user'] = $user;
        $products = $ProductModel->find();
      
        if($this->register){
            $TableModel = new Table();
            $ZoneModel = new Zone();
            $WaiterModel = new Waiter();
            $RegisterModel = new Register();
            $StockModel = new Stock();
            $tableModelName = $TableModel->select('name')->find($this->selectedTable);
           
            $data['header'] = $this->selectedTable ? ($this->selectedTable != '0h' ? "Ref:".'&nbsp'.$tableModelName['name'] : label("WalkinCustomer")) : label("WalkinCustomer");
           
            $tables = $TableModel->where('store_id',$this->store)->findAll();
            $zones = $ZoneModel->where('store_id',$this->store)->findAll();
            $waiters = $WaiterModel->where('store_id',$this->store)->findAll();
            
            $data['waiters'] = $waiters;
            $data['zones'] = $zones;
            
            $data['tables'] = $tables;
            $register = $RegisterModel->find($this->register);
            foreach ($products as $product) {
               if($product['type'] == '0'){
                  $stock = $StockModel->find('first', array('conditions' => array('store_id = ? AND product_id = ?', $register['store_id'], $product['id'])));
                  $price = $stock ? ($stock['price'] > 0 ? $stock['price'] : $product['price']) :$product['price'];
                  $product['price'] = $price;
               }
            }
        }
        date_default_timezone_set($this->setting['timezone']);
        $CustomerModel = new Customer();
        $CategoryModel = new Category();
        $StoreModel = new Store();
        $PosaleModel = new Posale();
        $data['customers'] = $CustomerModel->find();
        $data['products'] = $products;
        $data['categories'] = $CategoryModel->find();
        $data['Stores'] = $StoreModel->find();
        if (! $PosaleModel->first()) {
            $hold = $HoldModel->orderBy('id', 'desc')->limit(1)->first();
            if ($hold)
                $hold['time'] = date("H:i");
                
                $HoldModel->save($hold);
        }
        echo view('layouts/application',$data);
        echo view('pos', $data);
//        $this->content_view = 'pos';
    }

    public function change($type)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        
        $this->session->set('lang', $type);
        $this->setting->language = $type;
        $this->setting->save();
        redirect("", "refresh");
    }
}
