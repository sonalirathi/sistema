<?php
namespace App\Controllers;
use App\Models\categorie_expence;
use App\Models\Expence;
use App\Models\Setting;
use App\Models\Store;
use App\Models\User;

class Expences_controller extends BaseController
{
    public function ajax_list()
    {
         if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $UserModel = new User();
        $SettingModel = new Setting();
        $ExpenceModel = new Expence();
        $StoreModel = new Store();
        $categorie_expenceModel = new categorie_expence();
        
        $this->user = $this->session->get('user_id') ? $UserModel->find($this->session->get('user_id')) : FALSE;
        $lang = $this->session->get("lang") == null ? "english" : $this->session->get("lang");
//        $this->lang->load($lang, $lang);

        $this->setting = $SettingModel->find(1);
        
        date_default_timezone_set($this->setting['timezone']);
        
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        if(isset($_REQUEST['order'])){
            $columnIndex = $_REQUEST['order'][0]['column']; // Column index
            $columnName = $_REQUEST['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = strtoupper($_REQUEST['order'][0]['dir']); // asc or desc
        }else{
            $columnName = 0;
            $columnSortOrder = 'ASC';
        }
        if($columnName == 0)
        {
            $columnName = 'date';
        }
        
        $total_count = $ExpenceModel
                ->select("$ExpenceModel->table.*")
                ->select("$categorie_expenceModel->table.name as 'categorieName'")
                ->select("$StoreModel->table.name as 'storeName'")
                ->select("$UserModel->table.username as 'username'")
                ->orlike('date', $search_value)
                ->orLike('reference', $search_value)
                ->orLike('amount', $search_value)
                ->orLike("$categorie_expenceModel->table.name", $search_value)
                ->orLike("$StoreModel->table.name", $search_value)
                ->orLike("$UserModel->table.username", $search_value)
                ->join($categorie_expenceModel->table, "$categorie_expenceModel->table.id = $ExpenceModel->table.category_id", 'left')
                ->join($StoreModel->table, "$StoreModel->table.id = $ExpenceModel->table.store_id", 'left')
                ->join($UserModel->table, "$UserModel->table.id = $ExpenceModel->table.created_by", 'left')
                ->find();
            
        $list = $ExpenceModel
                ->select("$ExpenceModel->table.*")
                ->select("$categorie_expenceModel->table.name as 'categorieName'")
                ->select("$StoreModel->table.name as 'storeName'")
                ->select("$UserModel->table.username as 'username'")
                ->orlike('date', $search_value)
                ->orLike('reference', $search_value)
                ->orLike('amount', $search_value)
                ->orLike("$categorie_expenceModel->table.name", $search_value)
                ->orLike("$StoreModel->table.name", $search_value)
                ->orLike("$UserModel->table.username", $search_value)
                ->join($categorie_expenceModel->table, "$categorie_expenceModel->table.id = $ExpenceModel->table.category_id", 'left')
                ->join($StoreModel->table, "$StoreModel->table.id = $ExpenceModel->table.store_id", 'left')
                ->join($UserModel->table, "$UserModel->table.id = $ExpenceModel->table.created_by", 'left')
                ->orderBy($columnName,$columnSortOrder)
                ->limit($length,$start)
                ->find();
        
        
        $data = array();
        
        foreach ($list as $expence) {
            $start ++;
            $row = array();
            $row[] = $expence['date'];
            $row[] = $expence['reference'];
            $row[] = number_format((float)$expence['amount'], $this->setting['decimals'], '.', '');
             if(empty($expence['categorieName'])){
                $category = "-";
            }else{
                $category = $expence['categorieName'];
            }
            $row[] = $category;
            if(empty($expence['storeName'])){
                $store = "-";
            }else{
                $store = $expence['storeName'];
            }
            $row[] = $store;
            if(empty($expence['username'])){
                $username = "-";
            }else{
                $username = $expence['username'];
            }
            $row[] = $username;

            // add html for action
            if ($this->user['role'] === "admin")
                
                $row[] = '<div class="btn-group">
                      <a class="btn btn-default" href="javascript:void(0)" onclick="delete_expences(' . $expence['id'] . ')" title="' . label("Delete") . '"><i class="fa fa-times"></i></a>
                      <a class="btn btn-default" href="expences/edit/' . $expence['id'] . '" title="' . label("Edit") . '"><i class="fa fa-pencil"></i></a>
                      ' . ($expence['attachment'] ? '<a class="btn color02 white open-modalimage"  data-href="' .'../public/files/expences/' . $expence['attachment'] . '" title="' . label("ViewFile") . '" href="" data-toggle="modal" data-target="#ImageModal"><i class="fa fa-file-archive-o"></i></a>' : '') . '
                    </div>';
            else
                $row[] = '<div class="btn-group"><a class="btn btn-default" href="expences/edit/' . $expence['id'] . '" title="' . label("Edit") . '"><i class="fa fa-pencil"></i></a>
                      ' . ($expence['attachment'] ? '<a class="btn color02 white open-modalimage"  data-href="' . '../public/files/expences/' . $expence['attachment'] . '" title="' . label("ViewFile") . '" href="" data-toggle="modal" data-target="#ImageModal"><i class="fa fa-file-archive-o"></i></a>' : '') . '
                    </div>';

            $data[] = $row;
        }
        $output = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => count($total_count),
            "recordsFiltered" => count($total_count),
            "data" => $data
        );
       
        // output to json format
        echo json_encode($output);
    }

    public function ajax_delete($id)
    {
         if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $ExpenceModel = new Expence();
        $expence = $ExpenceModel->find($id);
        if ($expence['attachment'] !== '') {
            unlink('../public/files/expences/' . $expence['attachment']);
        }
        $ExpenceModel->delete($id);
        echo json_encode(array(
            "status" => TRUE
        ));
    }
}
