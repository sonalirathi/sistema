<?php
namespace App\Controllers;
use App\Models\Warehouse;
use App\Models\User;
use App\Models\Store;
use App\Models\Setting;

class Settings extends BaseController
{
    protected $session;
    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
       
        $WarehouseModel = new Warehouse();
        $UserModel = new User();
        $StoreModel = new Store();
        $SettingModel = new Setting();
        
        $data['warehouses'] = $WarehouseModel->find();
        $data['Users'] = $UserModel->find();
        $data['stores'] = $StoreModel->find();
        
        $data['Timezones'] = $this->tz_list();
        $data['setting'] = $SettingModel->find(1);
        echo view('layouts/application',$data);
        echo view('setting/setting',$data);
    }

    public function deleteUser($id)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $UserModel = new User();
        $user = $UserModel->find($id);
        unlink('./files/Avatars/' . $user['avatar']);
        $UserModel->delete($id);
        return redirect()->to('/settings?tab=users');
    }

    public function addUser()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $UserModel = new User();
        $SettingModel = new Setting();
        
        $this->setting = $data['setting'] = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
       
        $image = "";
        $avatar = $this->request->getFile('userfile');
        $fileName = '';
        $getfile = $avatar->getfileName();

        if(!empty($getfile)){
            $image = $avatar->getRandomName();
            $attachment = $avatar->move('../public/files/Avatars/', $image);
        }

        $_POST['avatar'] = $image;
        $_POST['created_at'] = $date;
        unset($_POST['PasswordRepeat']);
        
        $user = $UserModel->insert($_POST);
        return redirect()->to('/settings?tab=users');
    }

    public function editUser($id = FALSE)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $UserModel = new User();
        $StoreModel = new Store();
        $SettingModel = new Setting();
      
        $this->setting = $data['setting'] = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        if ($_POST) {
            $config['upload_path'] = './files/Avatars/';
            $config['encrypt_name'] = TRUE;
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_width'] = '1000';
            $config['max_height'] = '1000';

            $user = $UserModel->find($id);

           
            $avatar = $this->request->getFile('userfile');
            $fileName = '';
            $getfile = $avatar->getfileName();
            
            if(!empty($getfile)){
                $image = $avatar->getRandomName();
                $attachment = $avatar->move('../public/files/Avatars/', $fileName);
                if ($user['attachment'] !== '') {
                    unlink('../public/files/Avatars/' . $user['avatar']);
                }
                $_POST['avatar'] = $image;
            }
            $_POST['created_at'] = $date;
            unset($_POST['PasswordRepeat']);
            if ($_POST['password'] === '')
                unset($_POST['password']);
            $UserModel->update($id,$_POST);
            return redirect()->to('/settings?tab=users');
            
        } else {
            $data['stores'] = $StoreModel->find();
            $data['user'] = $UserModel->find($id);
            echo view('layouts/application',$data);
            echo view('setting/modifyUser',$data);
        }
    }

    // Settings
    public function updateSettings()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $UserModel = new User();
        $SettingModel = new Setting();
      
        $this->setting = $data['setting'] = $SettingModel->find(1);
        
        $config['upload_path'] = './files/Setting/';
        $config['encrypt_name'] = TRUE;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_width'] = '1000';
        $config['max_height'] = '1000';

        $setting = $SettingModel->find(1);

        $avatar = $this->request->getFile('userfile');
       
        $getfile = $avatar->getfileName();

        if(!empty($getfile)){
            $fileName = $avatar->getRandomName();
            $avatar->move('../public/files/Setting/', $fileName);
            $_POST['logo'] = $fileName;
             if ($setting['logo'] !== '') {
                unlink('../public/files/Setting/' . $setting['logo']);
            }
        }
        $SettingModel->update($setting['id'],$_POST);
        
        return redirect()->to('/settings?tab=setting');
    }
}
