<?php
namespace App\Controllers;

use App\Models\Store;
use App\Models\Setting;
use App\Models\Table;
use App\Models\Zone;

class Stores extends BaseController
{

     protected $session;

    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $SettingModel = new Setting();
        $StoreModel = new Store();
        
        $view_data['setting'] = $SettingModel->find(1);
        $view_data['Stores'] = $StoreModel->find();
        echo view('layouts/application',$view_data);
        echo view('stores/view',$view_data);
      
  }

    public function add()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $StoreModel = new Store();
        $SettingModel = new Setting();
        
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        $_POST['created_at'] = $date;
        $store = $StoreModel->insert($_POST);
        return redirect()->route('stores');
    }

    public function edit($id = FALSE)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $StoreModel = new Store();
        $SettingModel = new Setting();
        if ($_POST) {
            $StoreModel->update($id,$_POST);
            return redirect()->route('stores');
        } else {
            $view_data['store'] = $StoreModel->find($id);
            $view_data['setting'] = $SettingModel->find(1);
            echo view('layouts/application',$view_data);
            echo view('stores/modifyStore',$view_data);
        }
    }


    public function delete($id)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $StoreModel = new Store();
        $store = $StoreModel->delete($id);
        return redirect()->route('stores');
    }

    /**************************************************** table & zone functions ************************************************/

    public function editTables($id = FALSE)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $TableModel = new Table();
        $ZoneModel = new Zone();
        $SettingModel = new Setting();
        $tables = $TableModel->where('store_id',$id)->find();
        $zones = $ZoneModel->where('store_id', $id)->find();
        $zones02 = array();
        foreach ($zones as $zone) {
           $zones02[$zone['id']] = $zone['name'];
        }
        $view_data['storeid'] = $id;
        $view_data['tables'] = $tables;
        $view_data['zones'] = $zones;
        $view_data['zones02'] = $zones02;
        $view_data['setting'] = $SettingModel->find(1);
        echo view('layouts/application',$view_data);
        echo view('stores/tables',$view_data);
   }

   public function addTable()
   {
       if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $TableModel = new Table();
        $table = $TableModel->insert($_POST);
        return redirect()->to('/stores/editTables/'.$_POST['store_id']);
   }

    public function addzone()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $ZoneModel = new Zone();
        $zone = $ZoneModel->insert($_POST);
        return redirect()->to('/stores/editTables/'.$_POST['store_id']);
    }

    public function editzone()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $ZoneModel = new Zone();
        $zone = $ZoneModel->find($_POST['zone_id']);
        $zone['name'] = $_POST['name'];
        $ZoneModel->update($_POST['zone_id'],$zone);
         return redirect()->to('/stores/editTables/'.$_POST['store_id']);
   }

    public function deletetable()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $TableModel = new Table();
        $tables = $TableModel->where('id',$_POST['table_id'])->delete();
        return redirect()->to('/stores/editTables/'.$_POST['store_id']);
    }

    public function deletezone($id, $storeid)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $TableModel = new Table();
        $ZoneModel = new Zone();
        $tables = $TableModel->where('zone_id',$id)->delete();
        $zone = $ZoneModel->delete($id);
        return redirect()->to('/stores/editTables/'.$storeid);

    }

    public function editStoreTable($id = FALSE)
    {
       if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $TableModel  = new Table();
        $ZoneModel  = new Zone();
        $SettingModel  = new Setting();
        if ($_POST) {
            $table = $TableModel->find($id);
            $TableModel->update($id, $_POST);
             return redirect()->to('/stores/editTables/'.$table['store_id']);
        }else {
            $table = $TableModel->find($id);
            $zones = $ZoneModel->where('store_id',$table['store_id'])->find();
            $view_data['zones'] = $zones;
            $view_data['table'] = $table;
            $view_data['setting'] = $SettingModel->find(1);
            echo view('layouts/application',$view_data);
            echo view('stores/editTable',$view_data);
       }
        
    }
   
}
