<?php
namespace App\Controllers;

use App\Models\Customer;
use App\Models\Setting;

class Customers extends BaseController
{
    protected $session;

    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $CustomerModel = new Customer();
        $SettingModel = new Setting();
        
        $view_data['customers'] = $CustomerModel->find();
        $view_data['setting'] = $SettingModel->find(1);
      
        echo view('layouts/application',$view_data);
        echo view('customer/view',$view_data);
    }

    public function add()
    {
        $SettingModel = new Setting();
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $CustomerModel = new Customer();
         
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        $_POST['created_at'] = $date;
        $customer = $CustomerModel->insert($_POST);
        return redirect()->route('customers');
    }

    public function edit($id = FALSE)
    {
        $SettingModel = new Setting();
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $CustomerModel = new Customer();
        if ($_POST) {
            $CustomerModel->update($id,$_POST);
            return redirect()->route('customers');
        } else {
            $view_data['customer'] = $CustomerModel->find($id);
            $view_data['setting'] = $SettingModel->find(1);
            echo view('layouts/application',$view_data);
            echo view('customer/edit',$view_data);
        }
    }

    public function delete($id)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $CustomerModel = new Customer();
        $customer = $CustomerModel->delete($id);
        return redirect()->route('customers');
    }
}
