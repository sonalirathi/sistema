<?php
namespace App\Controllers;
use App\Models\Store;
use App\Models\Waiter;
use App\Models\Setting;


class Waiters extends BaseController
{
    protected $session;

    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $StoreModel = new Store();
        $WaiterModel = new Waiter();
        $SettingModel = new Setting();
        
        $stores = $StoreModel->find();
        $strs = array();
        foreach ($stores as $store) {
           $strs[$store['id']] = $store['name'];
        }
        $view_data['stores'] = $stores;
        $view_data['strs'] = $strs;
        $view_data['setting'] = $SettingModel->find(1);
        $view_data['waiters'] = $WaiterModel->find();
        echo view('layouts/application',$view_data);
        echo view('waiter/view',$view_data);
        
    }

    public function add()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $WaiterModel = new Waiter();
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        $_POST['created_at'] = $date;
        $waiter = $WaiterModel->insert($_POST);
        return redirect()->route('waiters');
    }

    public function edit($id = FALSE)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $WaiterModel = new Waiter();
        $StoreModel = new Store();
        $SettingModel = new Setting();
        if ($_POST) {
            $waiter = $WaiterModel->update($id, $_POST);
            return redirect()->route('waiters');
        } else {
            $view_data['waiter'] = $WaiterModel->find($id);
            $view_data['stores'] = $StoreModel->find();
            $view_data['setting'] = $SettingModel->find(1);
            echo view('layouts/application',$view_data);
            echo view('waiter/edit',$view_data);
        }
    }

    public function delete($id)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $WaiterModel = new Waiter();
        $waiter = $WaiterModel->delete($id);
        return redirect()->route('waiters');
    }
}
