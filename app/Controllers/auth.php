<?php
namespace App\Controllers;
use App\Models\Setting;
use App\Models\User;

class Auth extends BaseController
{
    public $session;
    function login()
    {

        $SettingModel = new Setting();
       
        $data['setting'] =  $SettingModel->find(1);
        
        if ($_POST) {

            $user = User::validate_login($this, $_POST['username'], $_POST['password']);
            if ($user) {
                return redirect()->route('dashboard');
            } else {
                $data['username'] = $_POST['username'];
                $data['message'] = label('login_incorrect');
            }
        }
        echo view('layouts/login', $data);
    }

    function logout()
    {
      
        if (! $this->session->get('logged_in')) {
            redirect('login');
        } else {
            $UserModel = new User();

            $data = [
                'last_active' => date("Y-m-d H:i:s")
            ];
            $id = $this->session->get('user_id');
            $user = $UserModel->update($id, $data);
  
            User::logout($this);
            return redirect()->route('/');
        }
    }
}
