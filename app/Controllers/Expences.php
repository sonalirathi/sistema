<?php
namespace App\Controllers;
use App\Models\Register;
use App\Models\Store;
use App\Models\Setting;
use App\Models\categorie_expence;
use App\Models\Expence;
use App\Models\User;
class Expences extends BaseController
{
    protected $session;

    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $RegisterModel = new Register();
        $StoreModel = new Store();
        $Categorie_expenceModel = new categorie_expence();
        $SettingModel = new Setting();
       
        
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        if ($this->register) {
            $Register = $RegisterModel->find($this->register);
            $store = $StoreModel->find($Register['store_id']);
            $data['storeName'] = $store['name'];
            $data['storeId'] = $store['id'];
        } else {
            $data['stores'] = $StoreModel->find();
        }
        $data['setting'] = $SettingModel->find(1);
        $data['categories'] = $Categorie_expenceModel->find();
       
        echo view('layouts/application',$data);
        echo view('expence/view',$data);
    }

    public function add()
    {
         if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
//        $config['upload_path'] = './files/expences/';
//        $config['encrypt_name'] = TRUE;
//        $config['overwrite'] = FALSE;
//        $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|doc|docx|xls|xlsx|zip';
//        $config['max_size'] = '2048';
        
         $ExpenceModel = new Expence();
        $fileName = "";
        $avatar = $this->request->getFile('file');
        $getfile = $avatar->getfileName();

        if(!empty($getfile)){
            $fileName = $avatar->getRandomName();
            $attachment = $avatar->move('../public/files/expences/', $fileName);
        }
        $data = array(
                "date" => isset($_POST['date']) ? $_POST['date'] : NULL,
                "reference" => isset($_POST['reference']) ? $_POST['reference'] : NULL,
                "category_id" => isset($_POST['category']) ? $_POST['category'] : NULL,
                "store_id" => isset($_POST['store_id']) ? $_POST['store_id'] : NULL,
                "amount" => isset($_POST['amount']) ? $_POST['amount'] : NULL,
                "note" => isset($_POST['note']) ? $_POST['note'] : NULL,
                "attachment" => $fileName,
                "created_by" => $this->session->get('user_id')
            );
            $expence = $ExpenceModel->insert($data);
             return redirect()->route('expences');
    }

    public function edit($id = FALSE)
    {
         if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
         $ExpenceModel = new Expence();
        if ($_POST) {
            $expence = $ExpenceModel->find($id);
            $data = array();
            $attachment = "";
            $avatar = $this->request->getFile('file');
            $fileName = '';
            $getfile = $avatar->getfileName();
            $data = array(
                "date" => isset($_POST['date']) ? $_POST['date'] : NULL,
                "reference" => isset($_POST['reference']) ? $_POST['reference'] : NULL,
                "category_id" => isset($_POST['category']) ? $_POST['category'] : NULL,
                "store_id" => isset($_POST['store_id']) ? $_POST['store_id'] : NULL,
                "amount" => isset($_POST['amount']) ? $_POST['amount'] : NULL,
                "note" => isset($_POST['note']) ? $_POST['note'] : NULL,
                
                "created_by" => $this->session->get('user_id')
                );
            if(!empty($getfile)){
                $fileName = $avatar->getRandomName();
                $data["attachment"] = $fileName;
                $attachment = $avatar->move('../public/files/expences/', $fileName);
                if ($expence['attachment'] !== '') {
                    unlink('../public/files/expences/' . $expence['attachment']);
                }
            }
          
            
            
            $ExpenceModel->update($id,$data);
             return redirect()->route('expences');
            
        } else {
            $StoreModel = new Store();
            $SettingModel = new Setting();
            $Categorie_expenceModel = new categorie_expence();
            $UserModel = new User();
            $expence = $ExpenceModel->find($id);
            $Viewdata = array();
            $store = $expence['store_id'] == 0 ? FALSE : $StoreModel->find($expence['store_id']);
            $Viewdata['storeName'] = $store ? $store['name'] : 'Store';
            $Viewdata['stores'] = $StoreModel->find();
            $Viewdata['categories'] = $Categorie_expenceModel->find();
            $Viewdata['user'] = $UserModel->find($this->session->get('user_id'));
            $Viewdata['expence'] = $expence;
            $Viewdata['setting'] = $SettingModel->find(1);
            echo view('layouts/application',$Viewdata);
            echo view('expence/edit',$Viewdata);
           
        }
    }
}
