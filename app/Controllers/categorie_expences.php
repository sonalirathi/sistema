<?php
namespace App\Controllers;
use App\Models\categorie_expence;
use App\Models\Setting;
use App\Models\User;

class Categorie_expences extends BaseController
{

    protected $session;

    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $SettingModel = new Setting();
        $Categorie_expenceModel = new Categorie_expence();
        $UserModel = new User();
        
        $view_data['setting'] = $SettingModel->find(1);
        $view_data['user'] = $UserModel->find($this->session->get('user_id'));
        $view_data['categories'] = $Categorie_expenceModel->find();
        echo view('layouts/application',$view_data);
        echo view('categorie_expence/view',$view_data);

    }

    public function add()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $Categorie_expenceModel = new Categorie_expence();
        $Categorie_expenceModel->insert($_POST);
        return redirect()->route('categorie_expences');
    }

    public function edit($id = FALSE)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $Categorie_expenceModel = new Categorie_expence();
        
        if ($_POST) {
            $category = $Categorie_expenceModel->update($id, $_POST);
            return redirect()->route('categorie_expences');
        } else {
            $SettingModel = new Setting();
        
            $view_data['setting'] = $SettingModel->find(1);
            $view_data['category'] = $Categorie_expenceModel->find($id);
            echo view('layouts/application',$view_data);
            echo view('categorie_expence/edit',$view_data);
        
        }
    }

    public function delete($id)
    {
         if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $Categorie_expenceModel = new Categorie_expence();
        $category = $Categorie_expenceModel->delete($id);
        return redirect()->route('categorie_expences');
    }
}
