<?php

namespace App\Controllers;

use App\Models\Supplier;
use App\Models\Setting;
use App\Models\Product;
use App\Models\Category;
use App\Models\User;
use App\Models\Stock;
use App\Models\Combo_item;

class Products extends BaseController
{

    protected $session;

    public function index()
    {

        if (!$this->session->get('logged_in'))
        {
            return redirect()->route('/');
        }
        
        $supplier = isset($_POST['filtersupp']) ? $_POST['filtersupp'] : '99';
        $supplierF = $supplier === '99' ? '99' : 'supplier';
        
        if (isset($_POST['filtersupp']) || isset($_POST['filtersupp']) === '0')
        {
            $type = $_POST['filtersupp'];
        } else
        {
            $type = '99';
        }

        $ProductModel = new Product();
        $CategoryModel = new Category();
        $SupplierModel = new Supplier();
        $SettingModel = new Setting();
        $UserModel = new User();
        $typeF = $type === '99' ? '99' : 'type';
        $conditions = array();
        if(!empty($supplier)){
            $conditions = [
                $supplierF => $supplier
            ];
        }
        if(!empty($type)){
            $conditions = [
                $typeF => $type
            ];
        }
      
        $view_data['products'] = $ProductModel->where($conditions)->find();
        
        //   $this->view_data['products'] = Product::all();
        $view_data['supplierF'] = $supplier;
        $view_data['typeF'] = $type;
        $view_data['categories'] = $CategoryModel->find();
        $view_data['suppliers'] = $SupplierModel->find();
        $view_data['setting'] = $SettingModel->find(1);
        $view_data['user'] = $this->session->get('user_id') ? $UserModel->find($this->session->get('user_id')) : FALSE;
        echo view('layouts/application', $view_data);
        echo view('product/view', $view_data);
    }

    public function csv()
    {
         if (!$this->session->get('logged_in'))
        {
            return redirect()->route('/');
        }
        
        $ProductModels = new Product();
        $result = $ProductModels->select(['code','name','category','cost','tax','description','price'])->find();
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"products".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        $headerArr = ['Code','Name','Category','Cost','Tax','Description','Price'];
        fputcsv($handle, $headerArr);
        foreach ($result as $data_array) {
            fputcsv($handle, $data_array);
        }
            fclose($handle);
        exit;
    }

    public function importcsv()
    {
        
//        $input = $this->validate([
//            'userfile' => 'uploaded[userfile]|max_size[file,1024]|ext_in[file,csv],'
//        ]);
//        if (!$input) { // Not valid
//            $data['validation'] = $this->validator;
//
//            return view('products',$data); 
//        }else{
//          
//        }
        $ProductModel = new Product();
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        if($file = $this->request->getFile('userfile')) {
            if ($file->isValid() && ! $file->hasMoved()) {
                // Get random file name
                $newName = $file->getRandomName();
                
                // Store file in public/csvfile/ folder
                $file->move('../public/files/products/', $newName);
                
                // Reading file
                $file = fopen("../public/files/products/".$newName,"r");
                $i = 0;
                $numberOfFields = 7; // Total number of fields

                $importData_arr = array();
                
                // Initialize $importData_arr Array
               while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                  $num = count($filedata);

                  // Skip first row & check number of fields
                  if($i > 0 && $num == $numberOfFields){ 

                     // Key names are the insert table field names - name, email, city, and status
                     $importData_arr[$i]['code'] = $filedata[0];
                     $importData_arr[$i]['name'] = $filedata[1];
                     $importData_arr[$i]['category'] = $filedata[2];
                     $importData_arr[$i]['cost'] = $filedata[3];
                     $importData_arr[$i]['tax'] = $filedata[4];
                     $importData_arr[$i]['description'] = $filedata[5];
                     $importData_arr[$i]['price'] = $filedata[6];

                  }

                  $i++;

               }
               fclose($file);
                // Insert data
                $count = 0;
                
                date_default_timezone_set($this->setting['timezone']);
                $date = date("Y-m-d H:i:s");
                foreach ($importData_arr as $prdct)
                {
                    $data = array(
                        "code" => $prdct['code'],
                        "name" => $prdct['name'],
                        "category" => $prdct['category'],
                        "cost" => $prdct['cost'],
                        "description" => $prdct['description'],
                        "tax" => $prdct['tax'],
                        "price" => $prdct['price'],
                        "color" => 'color01',
                        "photo" => '',
                        "created_at" => $date,
                        "modified_at" => $date
                    );
                    $ProductModel->insert($data);
                }
                unlink('../public/files/products/' . $newName);
            }
            
        }
        return redirect()->route("products");
        exit;
    }

    public function edit($id = FALSE)
    {
        $session = \Config\Services::session();
        $CategoryModel = new Category();
        $SupplierModel = new Supplier();
        $SettingModel = new Setting();
        $ProductModel = new Product();
        
        $Viewdata['categories'] = $CategoryModel->find();
        $Viewdata['suppliers'] = $SupplierModel->find();
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        if ($_POST)
        {
            $config['upload_path'] = './files/products/';
            $config['encrypt_name'] = TRUE;
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_width'] = '1000';
            $config['max_height'] = '1000';

            $product = $ProductModel->find($id);

            $data = array(
                    "type" => isset($_POST['type']) ? $_POST['type'] : NULL,
                    "code" => isset($_POST['code']) ? $_POST['code'] : NULL,
                    "name" => isset($_POST['name']) ? $_POST['name'] : NULL,
                    "category" => isset($_POST['category']) ? $_POST['category'] : NULL,
                    "cost" => isset($_POST['cost']) ? $_POST['cost'] : NULL,
                    "description" => isset($_POST['description']) ? $_POST['description'] : NULL,
                    "tax" => isset($_POST['tax']) ? $_POST['tax'] : NULL,
                    "alertqt" => isset($_POST['alertqt']) ? $_POST['alertqt'] : NULL,
                    "price" => isset($_POST['price']) ? $_POST['price'] : NULL,
                    "color" => isset($_POST['color']) ? $_POST['color'] : NULL,
                    "supplier" => isset($_POST['supplier']) ? $_POST['supplier'] : NULL,
                    "unit" => isset($_POST['unit']) ? $_POST['unit'] : NULL,
                    "taxmethod" => isset($_POST['taxmethod']) ? $_POST['taxmethod'] : NULL,
                    "options" => isset($_POST['options']) ? $_POST['options'] : NULL,
                    "created_at" => $date,
                    "modified_at" => $date
            );
            $avatar = $this->request->getFile('userfile');
                       
            $fileName = '';
            $getfile = $avatar->getfileName(); 
            
            if(!empty($getfile)){
                $fileName = $avatar->getRandomName();
                $data["photo"] = $fileName;
                $data["photothumb"] = $fileName;
                $attachment = $avatar->move('../public/files/products/', $fileName);
//                $attachment2 = $avatar2->move('../public/files/products/', $data["photothumb"]);
                if ($product['photo'] !== '') {
                    unlink('../public/files/products/' . $product['photo']);
                    unlink('../public/files/products/' . $product['photothumb']);
                }
            }
            
            $productSave = $ProductModel->update($id,$data);
            if($productSave){
                return redirect()->route("products");
            } else
            {
                $errorm = label('codeerror');
                $session->setFlashdata('error', $errorm);
                return redirect()->route("products/edit/" . $id);
            }
            
        } else
        {
            $Viewdata['product'] = $ProductModel->find($id);
            $Viewdata['setting'] = $SettingModel->find(1);
            $Viewdata['session'] = $session;
            echo view('layouts/application',$Viewdata);
            echo view('product/edit',$Viewdata);
        }
    }

    public function delete($id)
    {
        $ProductModel = new Product();
        $StockModel = new Stock();
        $Combo_itemModel = new Combo_item();
        $product = $ProductModel->find($id);
        if ($product['photo'] !== '')
        {
            unlink('./files/products/' . $product['photo']);
            unlink('./files/products/' . $product['photothumb']);
        }
        $stock = $StockModel->where('product_id', $id)->delete();
        $combos = $Combo_itemModel->where('product_id', $id)->delete();
        $ProductModel->delete($id);
        return redirect()->route('products');
    }

    function resize($path, $file)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        $config['create_thumb'] = TRUE;
        $config['maintain_thum'] = TRUE;
        $config['width'] = 120;
        $config['height'] = 120;
        $config['new_image'] = './files/products/' . $file;

        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

    function updatestock()
    {
        $StockModel = new Stock();
        $quant = isset($_POST['quant']) ? $_POST['quant'] : NULL;
        $quantw = isset($_POST['quantw']) ? $_POST['quantw'] : NULL;
        $pricest = isset($_POST['pricest']) ? $_POST['pricest'] : NULL;
        $productID = isset($_POST['productID']) ? $_POST['productID'] : NULL;
        if ($quant)
        {
            foreach ($quant as $qt)
            {
                $condition = array('store_id' => $qt['store_id'], 'product_id' =>$productID);
                if ($item = $StockModel->where($condition)->first())
                {
                    $item['quantity'] = $qt['quantity'];
                    $StockModel->update($item['id'],$item);
                } else
                {
                    $qt['product_id'] = $productID;
                    $StockModel->insert($qt);
                }
            }
        }
        if ($pricest)
        {
            foreach ($pricest as $pr)
            {
                $itemCondition = array('store_id' => $pr['store_id'], 'product_id' => $productID);
                if ($item = $StockModel->where($itemCondition)->first())
                {
                    $item['price'] = $pr['price'];
                    $StockModel->update($item['id'],$item);
                } else
                {
                    $pr['product_id'] = $productID;
                    $StockModel->insert($pr);
                }
            }
        }
        if ($quantw)
        {
            foreach ($quantw as $qt)
            {
                $itemCondition = array('warehouse_id' => $qt['warehouse_id'], 'product_id' => $productID);
                if ($item = $StockModel->where($itemCondition)->first())
                {
                    $item['quantity'] = $qt['quantity'];
                    $StockModel->update($item['id'],$item);
                } else
                {
                    $qt['product_id'] = $productID;
                    $StockModel->insert($pr);
                }
            }
        }
    }

}
