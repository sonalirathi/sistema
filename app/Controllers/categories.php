<?php
namespace App\Controllers;
use App\Models\Category;
use App\Models\Setting;
use App\Models\User;
class Categories extends BaseController
{
protected $session;

    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $SettingModel = new Setting();
        $CategoryModel = new Category();
        $UserModel = new User();
        $view_data['setting'] = $SettingModel->find(1);
        $view_data['categories'] = $CategoryModel->find();
        $view_data['user'] = $this->session->get('user_id') ? $UserModel->find($this->session->get('user_id')) : FALSE;
        echo view('layouts/application',$view_data);
        echo view('category/view',$view_data);
        
    }

    public function add()
    {
        $SettingModel = new Setting();
        $CategoryModel = new Category();
        
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        
        $date = date("Y-m-d H:i:s");
        $_POST['created_at'] = $date;
        $user = $CategoryModel->insert($_POST);
        return redirect()->route('categories');
    }

    public function edit($id = FALSE)
    {
        $SettingModel = new Setting();
        $CategoryModel = new Category();
        $view_data['setting'] = $SettingModel->find(1);
        if(!$this->session->get('logged_in')){
           return redirect()->route('/');
        }
        if ($_POST) {
            $category = $CategoryModel->update($id,$_POST);
            return redirect()->route('categories');
        } else {
            $view_data['setting'] = $SettingModel->find(1);
            $view_data['category'] = $CategoryModel->find($id);
            echo view('layouts/application',$view_data);
            echo view('category/edit',$view_data);
        
        }
    }

    public function delete($id)
    {
        $CategoryModel = new Category();
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $category = $CategoryModel->delete($id);
        return redirect()->route('categories');
    }
}
