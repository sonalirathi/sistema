<?php
namespace App\Controllers;
use App\Models\Sale;
use App\Models\Setting;
use App\Models\Expence;
use App\Models\Sale_item;
use App\Models\Warehouse;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Store;
use App\Models\Category;

class Stats extends BaseController
{

    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
       
        $CustomerModel = new Customer();
        $ProductModel = new Product();
        $StoreModel = new Store();
        $WarehouseModel = new Warehouse();
        $CategoryModel = new Category();
        $SaleModel = new Sale();
        $Sale_itemModel = new Sale_item();
        $ExpenceModel = new Expence();
        $SettingModel = new Setting();
        
        $this->setting = $data['setting'] = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d");
        $year = date("Y");
        
        $TodaySales = $SaleModel->select("sum(total) AS 'sum'")
                ->where('created_at', $date)
                ->first();
        $Top5product = $Sale_itemModel 
                 ->select("name")
                 ->select("product_id")
                ->select("sum(qt) AS 'totalquantity'")
                ->orderBy("SUM(qt)","DESC")
                ->groupBy("product_id")
                ->groupBy("name")
                ->where("DATE_FORMAT(date, '%Y')", $year)
                ->limit(5)
                ->find();

        $monthlySales = $SaleModel->getSaleDetails();
        $monthlyExp = $ExpenceModel->getExpenceDetails();
        
        $customer = $CustomerModel->find();
        $product =  $ProductModel->find();
        
        $data['customers'] = $customer;
        $data['Products'] = $product;
        $data['Stores'] = $StoreModel->find();
        $data['Warehouses'] = $WarehouseModel->find();
        $data['monthly'] = $monthlySales;
        $data['monthlyExp'] = $monthlyExp;
        $data['year'] = $year;
        $data['Top5product'] = $Top5product;
        $data['TodaySales'] = number_format((float)$TodaySales['sum'], $this->setting['decimals'], '.', '');
        $data['CustomerNumber'] = count($customer);
        $data['CategoriesNumber'] = count($CategoryModel->find());
        $data['ProductNumber'] = count($product);
        
        echo view('layouts/application',$data);
        echo view('stats',$data);
    }
}
