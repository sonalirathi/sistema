<?php
namespace App\Controllers;
use App\Models\Warehouse;
use App\Models\Setting;

class Warehouses extends BaseController
{
    protected $session;

    public function add()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $SettingModel = new Setting();
        $WarehouseModel = new Warehouse();
        
        $this->setting = $data['setting'] = $SettingModel->find(1);
        
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        $_POST['created_at'] = $date;
        $warehouse = $WarehouseModel->insert($_POST);
        return redirect()->to('/settings?tab=warehouses');
    }

    public function edit($id = FALSE)
    {
        $WarehouseModel = new Warehouse();
        $SettingModel = new Setting();
        
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        if ($_POST) {
            $WarehouseModel->update($id,$_POST);
            return redirect()->to('/settings?tab=warehouses');
        } else {
            $view_data['warehouse'] = $WarehouseModel->find($id);
            $view_data['setting'] = $SettingModel->find(1);
            echo view('layouts/application',$view_data);
            echo view('setting/modifyWarehouse',$view_data);
        }
    }

    public function delete($id)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $WarehouseModel = new Warehouse();
        $warehouse = $WarehouseModel->delete($id);
        return redirect()->to('/settings?tab=warehouses');
    }
}
