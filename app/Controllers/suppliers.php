<?php
namespace App\Controllers;
use App\Models\Supplier;
use App\Models\Setting;

class Suppliers extends BaseController
{
    protected $session;
    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $SettingModel = new Setting();
        $SupplierModel = new Supplier();
        $view_data['setting'] = $SettingModel->find(1);
        $view_data['suppliers'] = $SupplierModel->find();
        echo view('layouts/application',$view_data);
        echo view('supplier/view',$view_data);
    }

    public function add()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $SettingModel = new Setting();
        $SupplierModel = new Supplier();
        
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s");
        $_POST['created_at'] = $date;
        $supplier = $SupplierModel->insert($_POST);
        return redirect()->route('suppliers');
    }

    public function edit($id = FALSE)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('suppliers');
        }
        $SettingModel = new Setting();
        $SupplierModel = new Supplier();
        $view_data['setting'] = $SettingModel->find(1);
        if ($_POST) {
            $supplier = $SupplierModel->update($id, $_POST);
            return redirect()->route('suppliers');
        } else {
            $view_data['supplier'] = $SupplierModel->find($id);
            $view_data['setting'] = $SettingModel->find(1);
            echo view('layouts/application',$view_data);
            echo view('supplier/edit',$view_data);
        }
    }

    public function delete($id)
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $SupplierModel = new Supplier();
        $supplier = $SupplierModel->delete($id);
        return redirect()->route('suppliers');
    }
}
