<?php
namespace App\Controllers;
use App\Models\User;
use App\Models\Setting;
use App\Models\Sale;
use App\Models\Product;
use App\Models\Sale_item;
use App\Models\Expence;
use App\Models\Register;
use App\Models\Stock;

class Reports extends BaseController
{

    public function __construct()
    {
//        parent::__construct();
//        $UserModel = new User();
//        
//        $lang = $this->session->get("lang") == null ? "english" : $this->session->get("lang");
////        $this->lang->load($lang, $lang);
//        $this->user = $this->session->get('user_id') ? $UserModel->find($this->session->get('user_id')) : FALSE;
//
//        
    }

    public function getCustomerReport()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
        $SaleModel = new Sale();
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        
        $client_id = isset($_POST['client_id']) ? $_POST['client_id'] : NULL;
        $start = isset($_POST['start']) ? $_POST['start'] : NULL;
        $end = isset($_POST['end']) ? $_POST['end'] : NULL;
        $totals = 0;
        $sales = $SaleModel->getSaleCustomerReport($client_id, $start, $end);
        $result = '<table id="Table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr><th>' . label("Number") . '</th><th>' . label("CustomerName") . '</th><th>' . label("Discount") . '</th><th>' . label("Total") . '</th><th>' . label("Createdby") . '</th><th>' . label("TotalItems") . '</th></tr></thead><tbody>';
        foreach ($sales as $sale) {
            $result .= '<tr><td>' . $sale['id'] . '</td><td>' . $sale['clientname'] . '</td><td>' . $sale['discount'] . '</td><td>' . number_format((float)$sale['total'], $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td><td>' . $sale['created_by'] . '</td><td>' . $sale['totalitems'] . '</td></tr>';
            $totals += $sale['total'];
        }
        $result .= '</tbody>
      </table>
      <h1>' . label("Total") . ' : <span class="ReportTotal">' . $totals . ' ' . $this->setting['currency'] . '</span></h1>';

        echo $result;
    }

    public function getProductReport()
    {
        $ProductModel = new Product();
        $Sale_itemModel = new Sale_item();
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        
        $product_id = isset($_POST['product_id']) ? $_POST['product_id'] : NULL;
        $start = isset($_POST['start']) ? $_POST['start'] : NULL;
        $end = isset($_POST['end']) ? $_POST['end'] : NULL;
        $totalprofit = 0;
        $prduct = $ProductModel->find($product_id);
        $prducts = $Sale_itemModel->getSaleItemProductReport($product_id, $start, $end);
       
        $result = '<table id="Table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr><th>' . label("SaleNum") . '</th><th>' . label("ProductName") . '</th><th>' . label("Cost") . '</th><th>' . label("Price") . '</th><th>' . label("Quantity") . '</th><th>' . label("tax") . '</th><th>' . label("Total") . '</th><th>' . label("Profit") . '</th></tr></thead><tbody>';

        foreach ($prducts as $prd) {
            $tax = $prd['subtotal'] * intval(substr($prduct['tax'], 0, - 1)) / 100;
            $profit = $prd['subtotal'] - $tax - ($prduct['cost'] * $prd['qt']);
            $result .= '<tr><td>' . $prd['id'] . '</td><td>' . $prd['name'] . '</td><td>' . $prduct['cost'] . ' ' . $this->setting['currency'] . '</td><td>' . number_format((float)$prd['price'], $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td><td>' . $prd['qt'] . '</td><td>' . $prduct['tax'] . '(' . $tax . ' ' . $this->setting['currency'] . ')</td><td>' . $prd['subtotal'] . ' ' . $this->setting['currency'] . '</td><td>' . number_format((float)$profit, $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td></tr>';
            $totalprofit += $profit;
        }

        $result .= '</tbody></table><h1>' . label("TotalProfit") . ' : <span class="ReportTotal">' . $totalprofit . ' ' . $this->setting['currency'] . '</span></h1>';

        echo $result;
    }

    public function getRegisterReport()
    {
        $RegisterModel = new Register();
        $UserModel = new User();
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        
        $store_id = isset($_POST['store_id']) ? $_POST['store_id'] : NULL;
        $start = isset($_POST['start']) ? $_POST['start'] : NULL;
        $end = isset($_POST['end']) ? $_POST['end'] : NULL;
        
        $this->user = $this->session->get('user_id') ? $UserModel->find($this->session->get('user_id')) : FALSE;
        
        $TotalRevenue = 0;
        $register = $RegisterModel->getRegisterReport($store_id, $start, $end);
        
        $result = '<table id="Table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>
        <th>' . label("Openingtime") . '</th><th>' . label("closedat") . '</th><th>' . label("Openedby") . '</th>
        <th>' . label("Cash") . '</th><th>' . label("CreditCard") . '</th>
        <th>' . label("Cheque") . '</th><th> </th></tr></thead><tbody>';

        foreach ($register as $reg) {
            $useName = $UserModel->select('username')->first($reg['user_id']);
            $useName = $useName['username'];
           if($this->user['role'] === "admin")
            $action = '<div class="btn-group"><a class="btn btn-default" href="javascript:void(0)" onclick="delete_register(' . $reg['id'] . ')" title="' . label("Delete") . '"><i class="fa fa-times"></i></a></div>';
           else
            $action = '-';
            $result .= '<tr><td><a href="javascript:void(0)" '.($reg['closed_at'] ? 'onclick="RegisterDetails(' . $reg['id'] . ')"' : '').'>' . date('Y-m-d h:i:s', strtotime($reg['date'])) . '</a></td>
            <td>' . ($reg['closed_at'] ? $reg['closed_at'] : label("Stillopen")) . '</td>
            <td>' . $useName . '</td>
            <td>' . number_format((float)$reg['cash_total'], $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td>
            <td>' . number_format((float)$reg['cc_total'], $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td>
            <td>' . number_format((float)$reg['cheque_total'], $this->setting['decimals'], '.', '') . ' ' . $this->setting['currency'] . '</td>
            <td>'.$action.'</td></tr>';
            $TotalRevenue += $reg['cash_total'] + $reg['cheque_total'] + $reg['cc_total'];
        }

        $result .= '</tbody></table><h1>' . label("TotalRevenue") . ' : <span class="ReportTotal">' . $TotalRevenue . ' ' . $this->setting['currency'] . '</span></h1>';

        echo $result;
    }

    public function delete_register($id){
        $RegisterModel = new Register();
        $SaleModel = new Sale();
        $Sale_itemModel = new Sale_item();
        $PayementModel = new Payement();
        
        $register = $RegisterModel->find($id);
        $sales = $SaleModel->where('register_id', $id)->find();

        foreach ($sales as $sale) {
           $Sale_itemModel->where('sale_id',$sale['id'])->delete();
        }
        $SaleModel->where('register_id', $id)->delete();
        $PayementModel->where('register_id', $id)->delete();
        $RegisterModel->delete($id);
   }

    public function getyearstats($year)
    {
        $SaleModel = new Sale();
        $ExpenceModel = new Expence();
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        $monthly = $SaleModel->getSaleDetails();
        $monthlyExp = $ExpenceModel->getExpenceDetails();

        $result = '<table class="StatTable"><tr><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['januarytax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['januarydisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['january'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['january'] . ' ' . $this->setting['currency'] . '</span>' . label('January') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['feburarytax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['feburarydisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['feburary'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['feburary'] . ' ' . $this->setting['currency'] . '</span>' . label('February') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['marchtax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['marchdisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['march'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['march'] . ' ' . $this->setting['currency'] . '</span>' . label('March') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['apriltax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['aprildisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['april'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['april'] . ' ' . $this->setting['currency'] . '</span>' . label('April') . '</td></tr><tr><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['maytax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['maydisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['may'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['may'] . ' ' . $this->setting['currency'] . '</span>' . label('May') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['junetax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['junedisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['june'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['june'] . ' ' . $this->setting['currency'] . '</span>' . label('June') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['julytax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['julydisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['july'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['july'] . ' ' . $this->setting['currency'] . '</span>' . label('July') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['augusttax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['augustdisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['august'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['august'] . ' ' . $this->setting['currency'] . '</span>' . label('August') . '</td></tr><tr><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['septembertax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['septemberdisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['september'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['september'] . ' ' . $this->setting['currency'] . '</span>' . label('September') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['octobertax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['octoberdisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['october'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['october'] . ' ' . $this->setting['currency'] . '</span>' . label('October') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['novembertax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['novemberdisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['november'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['november'] . ' ' . $this->setting['currency'] . '</span>' . label('November') . '</td><td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>' . label('tax') . ' : <b>' . $monthly[0]['decembertax'] . ' ' . $this->setting['currency'] . '</b> <br><br> ' . label('Discount') . ' : <b>' . $monthly[0]['decemberdisc'] . ' ' . $this->setting['currency'] . '</b></h5>">' . $monthly[0]['december'] . ' ' . $this->setting['currency'] . '</span><span class="expencespan">' . $monthlyExp[0]['december'] . ' ' . $this->setting['currency'] . '</span>' . label('December') . '</td></tr></table>';

        echo $result;
    }

    /**
     * ****************** register functions ***************
     */
    public function RegisterDetails($id)
    {
        $RegisterModel = new Register();
        $UserModel = new User();
        $SettingModel = new Setting();
        $this->setting = $SettingModel->find(1);
        $register = $RegisterModel->find($id);
        try{$user = $UserModel->find($register['user_id']);}catch (\Exception $e){$user = "-";}
        try{$user2 = $UserModel->find($register['closed_by']);}catch (\Exception $e){$user2 = "-";}

        $CashinHand = number_format((float)$register['cash_inhand'], $this->setting['decimals'], '.', '');
        $date = $register['date'];
        $closedate = $register['closed_at'];
        $createdBy = $user['firstname'] . ' ' . $user['lastname'];
        $closedBy = $user2['firstname'] . ' ' . $user2['lastname'];
        $total = $register['cheque_total'] + $register['cc_total'] + $register['cash_total'];
        $subtotal = $register['cash_sub'] + $register['cc_sub'] + $register['cheque_sub'];

        $data = '<div class="col-md-3"><blockquote><footer>' . label("Openedby") . '</footer><p>' . $createdBy . '</p></blockquote></div>
        <div class="col-md-3"><blockquote><footer>' . label("CashinHand") . '</footer>
        <p>' . $CashinHand . ' ' . $this->setting['currency'] . '</p></blockquote></div><div class="col-md-4"><blockquote><footer>' . label("Openingtime") . '</footer>
        <p>' . $date->format('Y-m-d h:i:s') . '</p></blockquote></div><div class="col-md-2"><img src="' . site_url() . '/assets/img/register.svg" alt=""></div>
        <h2>' . label("PaymentsSummary") . '</h2><table class="table table-striped"><tr><th width="25%">' . label("PayementType") . '</th>
        <th width="25%">' . label("EXPECTED") . ' (' . $this->setting['currency'] . ')</th><th width="25%">' . label("COUNTED") . ' (' . $this->setting['currency'] . ')</th>
        <th width="25%">' . label("DIFFERENCES") . ' (' . $this->setting['currency'] . ')</th></tr><tr><td>' . label("Cash") . '</td>
        <td><span id="expectedcash">' . number_format((float)$register['cash_total'], $this->setting['decimals'], '.', '') . '</span></td>
        <td><span id="countedcash">' . number_format((float)$register['cash_sub'], $this->setting['decimals'], '.', '') . '</span></td><td>
        <span id="diffcash">' . number_format((float)($register['cash_sub'] - $register['cash_total']), $this->setting['decimals'], '.', '') . '</span></td>
        </tr><tr><td>' . label("CreditCard") . '</td>
        <td><span id="expectedcc">' . number_format((float)$register['cc_total'], $this->setting['decimals'], '.', '') . '</span></td>
        <td><span id="countedcc">' . number_format((float)$register['cc_sub'], $this->setting['decimals'], '.', '') . '</span></td>
        <td><span id="diffcc">' . number_format((float)($register['cc_sub'] - $register['cc_total']), $this->setting['decimals'], '.', '') . '</span></td>
        </tr><tr><td>' . label("Cheque") . '</td>
        <td><span id="expectedcheque">' . number_format((float)$register['cheque_total'], $this->setting['decimals'], '.', '') . '</span></td>
        <td><span id="countedcheque">' . number_format((float)$register['cheque_sub'], $this->setting['decimals'], '.', '') . '</span></td>
        <td><span id="diffcheque">' . number_format((float)($register['cheque_sub'] - $register['cheque_total']), $this->setting['decimals'], '.', '') . '</span></td></tr>
        <tr class="warning"><td>' . label("Total") . '</td>
        <td><span id="total">' . number_format((float)$total, $this->setting['decimals'], '.', '') . '</span></td>
        <td><span id="countedtotal">' . number_format((float)$subtotal, $this->setting['decimals'], '.', '') . '</span></td>
        <td><span id="difftotal">' . number_format((float)($subtotal - $total), $this->setting['decimals'], '.', '') . '</span></td></tr>
        </table><p>- ' . label("ClosedRegister") . ' ' . $closedBy . ' ' . label("at") . ' ' . $closedate . '</p><div  class="form-group"><h2>' . label("note") . '</h2><p>' . $register->note . '</p></div>';

        echo $data;
    }


    public function getStockReport()
    {
        $ProductModel = new Product();
        $StockModel = new Stock();
        $store_id = isset($_POST['stock_id']) ? $_POST['stock_id'] : NULL;
        $id = substr($store_id, 1);
        $products = $ProductModel->find();
        
        if(strcmp($store_id[0], "S"))
         $stype = 'warehouse_id';
        else
         $stype = 'store_id';
         // Stock::find('all', array('conditions' => array('store_id = ?', $id)));
        $result = '<table id="Table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>
        <th>' . label("Product") . ' ('.label("ProductCode").')</th>
        <th>' . label("Quantity") . '</th></tr></thead><tbody>';

        foreach ($products as $prod) {
           if($prod['type'] == '0'){
           $class = '';
           $stockCondition = array($stype => $id, 'product_id' => $prod['id']);
          
           if(!($stock = $StockModel->where($stockCondition)->first()))
            $stock = '-';
         else
            $stock = $stock['quantity'];

           if($stock < $prod['alertqt'])
              $class = 'danger';
            $result .= '<tr class='.$class.'>
            <td>' . $prod['name'] . ' ('.$prod['code'].')</td>
            <td>' . $stock . '</td></tr>';
            }
        }

        $result .= '</tbody></table>';

        echo $result;
    }
}
