<?php
namespace App\Controllers;
use App\Models\Table;
use App\Models\Zone;
use App\Models\Hold;
use App\Models\Posale;
use App\Models\Setting;
class Kitchens extends BaseController
{
    protected $session;
    
    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
        }
      
        $this->register =  $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->store =  $this->session->get('store') ? $this->session->get('store') : FALSE;
        
        $TableModel = new Table();
        $ZoneModel = new Zone();
        $HoldModel = new Hold();
        $PosaleModel = new Posale();
        $SettingModel = new Setting();
       
        $tables = $TableModel->where('store_id',$this->store)->find();
        $zones = $ZoneModel->where('store_id',$this->store)->find();
        foreach ($tables as $table) {
            if($table['status'] === 1){
                $holdConditions = array('register_id' => $this->register, 'table_id' => $table['id']);
                $hold = $HoldModel->where($holdConditions)->first();
                if($hold){
                    $posaleConditions = array('number' => 1, 'register_id' => $this->register, 'table_id' => $table['id']);
                    $posales = $PosaleModel->where($posaleConditions)->find();
                    foreach ($posales as $posale) {
                        $d1 = new DateTime($posale['time']);
                        $d2 = new DateTime($table['checked']);
                        if($d1 < $d2){
                          $table['time'] = 'y';
                        }else{
                          $table['time'] = 'n';
                          break;
                        }
                    }
                }
            }else{
                $table['time'] = 'y';
            }
        }
        $data['zones'] = $zones;
        $data['tables'] = $tables;
        $data['PosaleModel'] = $PosaleModel;
        $data['setting'] = $SettingModel->find(1);
        echo view('layouts/application',$data);
        echo view('kitchen',$data);
    
    }

}
