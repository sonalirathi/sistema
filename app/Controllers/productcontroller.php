<?php

namespace App\Controllers;

use App\Models\Warehouse;
use App\Models\Store;
use App\Models\Setting;
use App\Models\Product;
use App\Models\Stock;
use App\Models\Combo_item;
use Zend\Barcode\Barcode;
use Zend\Barcode\Object\Code128;
use Zend\Barcode\Renderer\Image;
class Productcontroller extends BaseController
{

    public function __construct()
    {
//        parent::__construct();
//        $this->user = $this->session->userdata('user_id') ? User::find_by_id($this->session->userdata('user_id')) : FALSE;
//        $lang = $this->session->userdata("lang") == null ? "english" : $this->session->userdata("lang");
//        $this->lang->load($lang, $lang);
//
//        $this->setting = Setting::find(1);
    }

    public function add()
    {
        if (!$this->session->get('logged_in'))
        {
            return redirect()->route('/');
        }
        $WarehouseModel = new Warehouse();
        $StoreModel = new Store();
        $SettingModel = new Setting();
        $ProductModel = new Product();
        $warehouses = $WarehouseModel->find();
        $stores = $StoreModel->find();
        $content = "";
        $type = isset($_POST['type']) ? $_POST['type'] : NULL;
        if ($type === '0')
        {
            $content .= '<div class="col-md-12"><table class="table table-striped"><thead><tr><th width="40%">' . label("Store") . '</th><th width="30%">' . label("Quantity") . '</th><th width="30%">' . label("price") . '</th></tr></thead><tbody class="itemslist">';
            foreach ($stores as $store)
            {
                $content .= '<tr><td>' . $store['name'] . '</td><td><input type="number" id="quantity" store-id="' . $store['id'] . '" value="0"></td><td><input type="number" id="pricestr" store-id="' . $store['id'] . '" value="0"></td></tr>';
            }
            $content .= '  </tbody></table><script type="text/javascript"> $( "[id=\'quantity\']" ).on("change", function() { var storeID = $(this).attr("store-id"); quant.push({ "store_id": storeID, "quantity": $(this).val() }); console.log(quant); }); $( "[id=\'quantityw\']" ).on("change", function() { var warehouseID = $(this).attr("warehouse-id"); quantw.push({ "warehouse_id": warehouseID, "quantity": $(this).val() }); }); $( "[id=\'pricestr\']" ).on("change", function() { var storeID = $(this).attr("store-id"); pricestore.push({ "store_id": storeID, "price": $(this).val() }); });</script></div>';

            $content .= '<div class="col-md-12"><table class="table table-striped"><thead><tr><th width="40%">' . label("Warehouses") . '</th><th width="30%">' . label("Quantity") . '</th><th width="30%">' . label("price") . '</th></tr></thead><tbody class="itemslist">';
            foreach ($warehouses as $warehouse)
            {
                $content .= '<tr><td>' . $warehouse['name'] . '</td><td><input type="number" id="quantityw" warehouse-id="' . $warehouse['id'] . '" value="0"></td><td><input type="number" id="price" value="" disabled="true"></td></tr>';
            }
            $content .= '  </tbody></table></div>';
        } elseif ($type === '2')
        {
            $content .= '<div><label for="add_item">' . label("AddProduct") . '</label><input type="text" id="add_item" class="col-md-12"></div><div><label for="Comboprd">' . label("combination") . '</label><table id="Comboprd" class="table items table-striped table-bordered table-condensed table-hover"><thead><tr><th class="col-xs-9">' . label("ProductName") . '</th><th class="col-xs-2">' . label("Quantity") . '</th><th class=" col-xs-1 text-center"><i class="fa fa-trash-o trash-opacity-50"></i></th></tr></thead><tbody></tbody></table></div>';
        }
        $this->setting = $SettingModel->find(1);
        date_default_timezone_set($this->setting['timezone']);
        $date = date("Y-m-d H:i:s"); 
        if ($_POST)
        {
            $config['upload_path'] = './files/products/';
            $config['encrypt_name'] = TRUE;
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_width'] = '1000';
            $config['max_height'] = '1000';

            $product = $ProductModel->find(); 
 
//            $image = $data['upload_data']['file_name'];
//            $image_thumb = $data['upload_data']['raw_name'] . '_thumb' . $data['upload_data']['file_ext'];
            $data = array(
                "type" => isset($_POST['type']) ? $_POST['type'] : NULL,
                "code" => isset($_POST['code']) ? $_POST['code'] : NULL,
                "name" =>isset($_POST['name']) ? $_POST['name'] : NULL,
                "category" =>isset($_POST['category']) ? $_POST['category'] : NULL,
                "cost" => isset($_POST['cost']) ? $_POST['cost'] : NULL,
                "description" =>isset($_POST['description']) ? $_POST['description'] : NULL,
                "tax" => isset($_POST['tax']) ? $_POST['tax'] : NULL,
                "alertqt" => isset($_POST['alertqt']) ? $_POST['alertqt'] : NULL,
                "price" => isset($_POST['price']) ? $_POST['price'] : NULL,
                "color" => isset($_POST['color']) ? $_POST['color'] : NULL,
                "supplier" => isset($_POST['supplier']) ? $_POST['supplier'] : NULL,
                "unit" => isset($_POST['unit']) ? $_POST['unit'] : NULL,
                "taxmethod" => isset($_POST['taxmethod']) ? $_POST['taxmethod'] : NULL,
                "options" => isset($_POST['options']) ? $_POST['options'] : NULL,
//                "photo" => $image,
//                "photothumb" => $image_thumb,
                "created_at" => $date,
                "modified_at" => $date
            );
            
            
            $avatar = $this->request->getFile('userfile');
            if(!empty($avatar)){
                $fileName = '';
                $getfile = $avatar->getfileName(); 

                if(!empty($getfile)){
                    $fileName = $avatar->getRandomName();
                    $data["photo"] = $fileName;
                    $data["photothumb"] = $fileName;
                    $attachment = $avatar->move('../public/files/products/', $fileName);
    //                $attachment2 = $avatar2->move('../public/files/products/', $data["photothumb"]);
    //                p($data["photo"]);
                    if ($data["photo"] !== '') {
                        unlink('../public/files/products/' . $data["photo"]);
    //                    unlink('../public/files/products/' . $product['photothumb']);
                    }

                }
            }
            $product = $ProductModel->insert($data);
//            p($product);
            $content .= '<input type="hidden" id="prodctID" value="' .$product . '">';
            if ($_POST['type']  === '0')
                echo $content;
            elseif ($_POST['type']  === '2')
                echo $content;
            else
                echo 'service';
//            redirect("products", "refresh");
        }
//        return redirect()->route('products');
    }

    
    public function modifystock($id)
    {
        $WarehouseModel = new Warehouse();
        $StoreModel = new Store();
        $StockModel = new Stock();
        
        $warehouses = $WarehouseModel->find();
        $stores = $StoreModel->find();

        $content = '<div class="col-md-12"><table class="table table-striped"><thead><tr><th width="40%">' . label("Store") . '</th><th width="30%">' . label("Quantity") . '</th><th width="30%">' . label("price") . '</th></tr></thead><tbody class="itemslist">';
        foreach ($stores as $store)
        {
            $condition = array('store_id' => $store['id'], 'product_id' => $id);
            $stock = $StockModel->where($condition)->first();
            $quantity = $stock ? $stock['quantity'] : '0';
            $price = $stock ? ($stock['price'] ? $stock['price'] : '0') : '0';
            $content .= '<tr><td>' . $store['name'] . '</td><td><input type="number" id="quantity" store-id="' . $store['id'] . '" value="' . $quantity . '"></td><td><input type="number" id="pricestr" store-id="' . $store['id'] . '" value="' . $price . '"></td></tr>';
        }
        $content .= '  </tbody>
       </table></div>';

        $content .= '<div class="col-md-12"><table class="table table-striped"><thead><tr><th width="40%">' . label("Warehouses") . '</th><th width="30%">' . label("Quantity") . '</th><th width="30%">' . label("price") . '</th></tr></thead><tbody class="itemslist">';
        foreach ($warehouses as $warehouse)
        {
            $stockCondition = array('warehouse_id' => $warehouse['id'], 'product_id' => $id);
            $stock = $StockModel->where($stockCondition)->first();
            $quantity = $stock ? $stock['quantity'] : '0';
            $content .= '<tr><td>' . $warehouse['name'] . '</td><td><input type="number" id="quantityw" warehouse-id="' . $warehouse['id'] . '" value="' . $quantity . '"></td><td><input type="number" id="pricew" value="" disabled="true"></td></tr>';
        }
        $content .= '  </tbody>
        </table></div>';
        $content .= '<input type="hidden" id="prodctID" value="' . $id . '">';
        echo $content;
    }

    public function Viewproduct($id)
    {
        $WarehouseModel = new Warehouse();
        $StoreModel = new Store();
        $ProductModel = new Product();
        $SettingModel = new Setting();
        $StockModel = new Stock();
        $Combo_itemModel = new Combo_item();
        
        $warehouses = $WarehouseModel->find();
        $stores = $StoreModel->find();
        $product = $ProductModel->find($id);
        $this->setting =  $SettingModel->find(1);

        if ($product['photo'])
            $photo = '<img class="media-object img-rounded" src="' . site_url() . '/files/products/' . $product['photo'] . '" alt="image" width="200px">';
        else
            $photo = '<img class="media-object img-rounded" src="http://dummyimage.com/200x200/e3e0e3/fff" alt="image" width="200px">';
        $content = '<div class="col-md-6"><div class="media"><div class="media-left">' . $photo . '</div><div class="media-body"><h1 class="media-heading">' . $product['name'] . '</h1><b>' . label("Category") . ' :</b> ' . $product['category'] . ' <br><b>' . label("Cost") . ' :</b> ' . $product['cost'] . ' ' . $this->setting['currency'] . '<br><b>' . label("ProductTax") . ' :</b> ' . $product['tax'] . ' <br><b>' . label("Price") . ' :</b> ' . $product['price'] . ' ' . $this->setting['currency'] . ' <br><b>' . label("Suppliers") . ' :</b> ' . $product['supplier'] . ' <br><b>' . label("ProductDescription") . ' :</b> ' . $product['description'] . '</div></div></div><div class="col-md-6"><table class="table"><thead><tr><th width="40%">' . label("Store") . '</th><th width="20%">' . label("Quantity") . '</th><th width="20%">' . label("price") . '</th><th width="20%"><i class="fa fa-eye" aria-hidden="true"></i></th></tr></thead><tbody class="itemslist">';
        foreach ($stores as $store)
        {
            if ($product['type'] == '0')
            {
                $condition = array('store_id' => $store['id'], 'product_id' => $id);
                $stock = $StockModel->where($condition)->first();
                $quantity = $stock ? $stock['quantity'] : '0';
                $price = $stock ? ($stock['price'] ? $stock['price'] . ' ' . $this->setting['currency'] : '0' . ' ' . $this->setting['currency']) : '0' . ' ' . $this->setting['currency'];
            } else
            {
                $quantity = '-';
                $price = '-';
            }
            /*             * *********************************************** store visibility value ************************************************************** */
            $cheked = 'checked';
            $invis = $product['h_stores'];
            $invis = trim($invis, ",");
            $array = explode(',', $invis); //split string into array seperated by ', '
            foreach ($array as $value) //loop over values
            {
                $cheked = $value == $store['id'] ? '' : $cheked;
            }
            $content .= '<tr>
             <td>' . $store['name'] . '</td>
             <td><b>' . $quantity . '</b></td>
             <td><b>' . $price . '</b></td>
             <td><label>
             <input type="checkbox" name="invis" value="1" ' . $cheked . ' onclick="makePrdInvis(' . $store['id'] . ', ' . $product['id'] . ')">
             <span class="label-text"></span>
             </label></td>
          </tr>';
        }
        $content .= '  </tbody>
      </table>

      <table class="table">
          <thead>
             <tr>
                <th width="40%">' . label("Store") . '</th>
                <th width="20%">' . label("Quantity") . '</th>
                <th width="20%">' . label("price") . '</th>
                <th width="20%"><i class="fa fa-eye" aria-hidden="true"></i></th>
             </tr>
          </thead>
          <tbody class="itemslist">';
        foreach ($warehouses as $warehouse)
        {
            if ($product['type'] == '0')
            {
                $stockCondition = array('warehouse_id' => $warehouse['id'], 'product_id' => $id);
                $stock = $StockModel->where($stockCondition)->first();
                $quantity = $stock ? $stock['quantity'] : '0';
            } else
            {
                $quantity = '-';
            }
            $content .= '<tr>
             <td>' . $warehouse['name'] . '</td>
             <td><b>' . $quantity . '</b></td>
             <td><b>-</b></td>
             <td><b>-</b></td>
          </tr>';
        }
        $content .= '  </tbody>
      </table></div>';
        if ($product['type'] == 2)
        {
            $combos = $Combo_itemModel->where('product_id',$id)->find();
            $content .= '<div class="row"><div class="col-md-12"><h1>' . label("combinations") . '</h1></div><div class="col-md-12"><table class="table">
             <thead>
                <tr>
                   <th class="col-xs-9">' . label("ProductName") . '</th>
                   <th class="col-xs-2">' . label("Quantity") . '</th>
                </tr>
             </thead>
             <tbody>';
            foreach ($combos as $combo)
            {
                $prod = $ProductModel->find($combo['item_id']);
                $content .= '<tr>
                  <td><a href="javascript:void(0)" onclick="Viewproduct(' . $combo['item_id'] . ')">' . $prod['name'] . ' (' . $prod['code'] . ')</a></td>
                  <td><b>' . $combo['quantity'] . '</b></td>
               </tr>';
            }
            $content .= '  </tbody>
           </table></div></div><button type="submit" class="btn btn-add col-md-12" style="margin-bottom:0" onclick="modifycombo(' . $id . ')">' . label("Modify") . '</button>';
        }
        echo $content;
    }

   
    function GenerateBarcode($code = NULL, $bcs = 'code128', $height = 60, $width = 1) {
        $barcode = new Code128([
            'text' => $code,
            'barHeight' => $height,
            'factor' => 2,
            'drawText' => FALSE
        ]);

        $renderer = new Image([
            'resource' => imagecreate($barcode->getWidth(), $barcode->getHeight()),
            'barcode' => $barcode,
        ]);
        
        ob_start();
        $renderer->render();
        $image = base64_encode(ob_get_contents());
        ob_end_clean();
    
        return $image;
    }

    public function barcode($row, $num, $productBcode)
    {
        $content = "";
        $bcs = 'code128';
        $height = 30;
        $width = 2;
        for ($i = 0; $i < $num; $i++)
        {
            $content .= '<div class="col-md-' . $row . '"><img style="margin-top:30px" src="data:image/png;base64,' .$this->GenerateBarcode(sprintf("%05d", $productBcode), $bcs, $height, $width). '" alt="' . $productBcode . '" /></div>';
        }

        echo $content;
    }

    /*     * **************************************** combo functions **************************************** */

    public function getProductNames($term)
    {
        $ProductModel = new Product();
        $condition = array();
        $prd = $ProductModel
                ->orlike('name', $term)
                ->orlike('code', $term)
                ->where('type','!=',2)
                ->find();
        if ($prd)
        {
            return $prd;
        }
        return FALSE;
    }

    public function suggest()
    {
        $term = isset($_GET['term']) ? $_GET['term'] : NULL;
        $rows = $this->getProductNames($term);
       
        if ($rows)
        {
            foreach ($rows as $row)
            {
                $pr[] = array('id' => $row['id'], 'label' => $row['name'], 'name' => $row['name'], 'code' => $row['code'], 'cost' => $row['cost']);
            }
            echo json_encode($pr);
        } else
        {
            echo json_encode(array('id' => 0, 'label' => label('NoProduct')));
        }
    }

    public function addcombo()
    {
        $items = isset($_POST['items']) ? $_POST['items'] : NULL;
        $productID = isset($_POST['productID']) ? $_POST['productID'] : NULL;
        $Combo_itemModel = new Combo_item();
        if ($items)
        {
            $combos = $Combo_itemModel->where('product_id', $productID)->delete();
            
            foreach ($items as $item)
            {
                $item['product_id'] = $productID;
                unset($item['code']);
                unset($item['name']);
                $Combo_itemModel->insert($item);
            }
        }
    }

    public function modifycombo($id)
    {
        $Combo_itemModel = new Combo_item();
        $combos = $Combo_itemModel->where('product_id', $id)->find();

        $content = '<div><label for="add_item">' . label("AddProduct") . '</label><input type="text" id="add_item" class="col-md-12"></div>
         <div>
         <label for="Comboprd">' . label("combination") . '</label>
         <table id="Comboprd" class="table items table-striped table-bordered table-condensed table-hover">
         <thead>
         <tr>
         <th class="col-xs-9">' . label("ProductName") . '</th>
         <th class="col-xs-2">' . label("Quantity") . '</th>
         <th class=" col-xs-1 text-center"><i class="fa fa-trash-o trash-opacity-50"></i></th>
         </tr>
         </thead>
         <tbody>';
        foreach ($combos as $combo)
        {
            $ProductModel = new Product();
            $prod = $ProductModel->find($combo['item_id']);
            $content .= '<tr id="rowid_' . $combo['item_id'] . '" class="item_' . $combo['item_id'] . '">
              <td>' . $prod['name'] . ' (' . $prod['code'] . ')</td>
              <td><b>' . $combo['quantity'] . '</b></td>
              <td><i class="fa fa-times tip delt" id="' . $combo['item_id'] . '" title="Remove" style="cursor:pointer;"></i></td>
              </tr>';
        }
        $content .= '</tbody>
         </table>
         </div>
         <input type="hidden" id="prodctID" value="' . $id . '">';

        echo $content;
    }

    public function getcombos($id)
    {
        $Combo_itemModel = new Combo_item();
        $ProductModel = new Product();
        $combos = $Combo_itemModel->where('product_id', $id)->find();

        if ($combos)
        {
            foreach ($combos as $row)
            {
                $prod = $ProductModel->find($row['item_id']);
                $pr[] = array('item_id' => $row['item_id'], 'quantity' => $row['quantity'], 'code' => $prod['code'], 'name' => $prod['name']);
            }
            echo json_encode($pr);
        } else
        {
            echo json_encode();
        }
    }

    /*     * ******************* make product invisible on a store ****************************** */

    public function makePrdInvis($id, $productId)
    {
        $ProductModel = new Product();
        $product = $ProductModel->find($productId);
        $cheked = false;
        $newVal = '';
        $invis = $product['h_stores'];
        $invis = trim($invis, ",");
        $array = explode(',', $invis); //split string into array seperated by ','
        foreach ($array as $value) //loop over values
        {
            if (intval($value) == intval($id))
            {
                $newVal = $newVal;
                $cheked = true;
            } else
            {
                $newVal .= $value . ',';
            }
        }
        if (!$cheked)
        {
            $product['h_stores'] .= $id . ',';
            $ProductModel->update($productId,$product);
        } else
        {
            $product['h_stores'] = $newVal;
            $ProductModel->update($productId,$product);
        }
        echo json_encode(array(
            "status" => TRUE
        ));
    }

}
