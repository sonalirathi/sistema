<?php
namespace App\Controllers;
use App\Models\Waiter;
use App\Models\Setting;
class Sales extends BaseController
{
    protected $session;

    public function index()
    {
        if(!$this->session->get('logged_in')){
            return redirect()->route('/');
            
        }
        $WaiterModel = new Waiter();
        $SettingModel = new Setting();
        $this->register = $this->session->get('register') ? $this->session->get('register') : FALSE;
        $this->store = $this->session->get('store') ? $this->session->get('store') : FALSE;
        $data['register'] = $this->register;
        if($this->register){
           $waiters = $WaiterModel->where('store_id', $this->store)->find();
           
        }else {
           $waiters = array();
        }
        $data['waiters'] = $waiters;
        $data['setting'] = $SettingModel->find(1);
        $user = array();
        $user['firstname'] = $this->session->get('firstname');
        $user['lastname'] = $this->session->get('lastname');
        $data['user'] = $user;
        echo view('layouts/application',$data);
        echo view('sale',$data);
    }
}
